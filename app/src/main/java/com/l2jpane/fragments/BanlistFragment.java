package com.l2jpane.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.l2jpane.App;
import com.l2jpane.R;
import com.l2jpane.database.DBConnection;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.ShowErrorEvent;
import com.l2jpane.singletone.CurrentServer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class BanlistFragment extends Fragment implements View.OnClickListener {

    private final int UPLOAD_STATUS = -1;
    private final int DOWNLOAD_STATUS = -2;

    private EditText banlistET;
    private ProgressDialog pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_banlist, container, false);
        Button downloadBtn = (Button) view.findViewById(R.id.downloadBtn);
        Button uploadBtn = (Button) view.findViewById(R.id.uploadBtn);
        banlistET = (EditText) view.findViewById(R.id.banlistET);
        downloadBtn.setOnClickListener(this);
        uploadBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        pd = new ProgressDialog(getContext());
        pd.setTitle(getString(R.string.please_wait));
        pd.show();
        switch (v.getId()) {
            case R.id.downloadBtn:
                pd.setMessage(getActivity().getString(R.string.download_banlist));
                new UploadDownloadTask(DOWNLOAD_STATUS).execute();
                break;
            case R.id.uploadBtn:
                pd.setMessage(getActivity().getString(R.string.upload_banlist));
                new UploadDownloadTask(UPLOAD_STATUS).execute();
                break;
        }
    }

    private class UploadDownloadTask extends AsyncTask<Void, Void, String> {

        private int status;
        private String text;

        private UploadDownloadTask(int _status) {
            status = _status;
        }

        @Override
        protected void onPreExecute() {
            if (status == UPLOAD_STATUS)
                text = banlistET.getText().toString();
        }

        @Override
        protected String doInBackground(Void... params) {

            String path = "";
            if (CurrentServer.getInstance().getSelectedServerCFG().containsKey("gs_pathBanlist"))
                path = CurrentServer.getInstance().getSelectedServerCFG().get("gs_pathBanlist").toString();
            Session session = null;
            if (status == DOWNLOAD_STATUS) {
                try {
                    session = DBConnection.getSSHSession();
                    String banlist = "";
                    Channel channel = session.openChannel("sftp");
                    channel.connect();
                    ChannelSftp c = (ChannelSftp) channel;
                    BufferedReader bis = new BufferedReader(new InputStreamReader(c.get(path)));
                    String line = null;
                    while ((line = bis.readLine()) != null) {
                        banlist = banlist + line + "\n\n";
                    }
                    return banlist;
                } catch (JSchException | SftpException | IOException e) {
                    String fullError = "";
                    String shortError = e.getMessage();
                    for (StackTraceElement s : e.getStackTrace()) {
                        fullError += s.toString() + "\n";
                    }
                    BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                    e.printStackTrace();
                }

            } else if (status == UPLOAD_STATUS) {
                try {
                    FileOutputStream output = getContext().openFileOutput("banlist", Context.MODE_PRIVATE);
                    DataOutputStream dout = new DataOutputStream(output);
                    text = text.replaceAll("\n\n", "\n");
                    dout.writeUTF(text);
                    dout.flush();
                    dout.close();
                    session = DBConnection.getSSHSession();
                    Channel channel = session.openChannel("sftp");
                    channel.connect();

                    ChannelSftp c = (ChannelSftp) channel;
                    c.put(App.getContext().getApplicationContext().getFilesDir().getAbsolutePath() + "/banlist", path);
                    return getString(R.string.saved);
                } catch (IOException | JSchException | SftpException e) {
                    String fullError = "";
                    String shortError = e.getMessage();
                    for (StackTraceElement s : e.getStackTrace()) {
                        fullError += s.toString() + "\n";
                    }
                    BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String text) {
            banlistET.setText(text);
            pd.dismiss();
        }
    }
}
