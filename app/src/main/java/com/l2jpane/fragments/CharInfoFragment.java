package com.l2jpane.fragments;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.l2jpane.L2Resources;
import com.l2jpane.R;
import com.l2jpane.database.SQL;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.ShowErrorEvent;
import com.l2jpane.eventbus.ShowLoadingEvent;
import com.l2jpane.eventbus.StartInfoLoadingEvent;
import com.l2jpane.eventbus.StartInventoryLoadingEvent;
import com.l2jpane.singletone.CurrentServer;
import com.squareup.otto.Subscribe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CharInfoFragment extends Fragment {

    private GridView infoGV;
    private SimpleAdapter sAdapter;
    private boolean altCharInfo = false;
    private boolean altCreateTime = false;
    private boolean enableCreateTime = false;
    private ProgressBar cpPB;
    private ProgressBar hpPB;
    private ProgressBar mpPB;
    private TextView statusTV;
    private TextView cpTV;
    private TextView hpTV;
    private TextView mpTV;
    private TextView lastAccessTV;
    private String nick;
    private int status = 0;
    private long lastAccess = -1;
    private float currentCP = -1;
    private float maxCP = -1;
    private float currentHP = -1;
    private float maxHP = -1;
    private float currentMP = -1;
    private float maxMP = -1;
    private long charId = 0;
    private long clanId = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        altCreateTime = CurrentServer.getInstance().getCurrentBuild().get("altCreateTime").equals("true");
        altCharInfo = CurrentServer.getInstance().getCurrentBuild().get("altCharInfo").equals("true");
        enableCreateTime = CurrentServer.getInstance().getCurrentBuild().get("enableCreateTime").equals("true");
        View view = inflater.inflate(R.layout.fragment_char_info, container, false);
        infoGV = (GridView) view.findViewById(R.id.infoGV);
        cpPB = (ProgressBar) view.findViewById(R.id.cpPB);
        hpPB = (ProgressBar) view.findViewById(R.id.hpPB);
        mpPB = (ProgressBar) view.findViewById(R.id.mpPB);
        statusTV = (TextView) view.findViewById(R.id.statusTV);
        cpTV = (TextView) view.findViewById(R.id.cpTV);
        hpTV = (TextView) view.findViewById(R.id.hpTV);
        mpTV = (TextView) view.findViewById(R.id.mpTV);
        lastAccessTV = (TextView) view.findViewById(R.id.lastAccessTV);
        if (sAdapter != null)
            fillInfo();
        nick = getActivity().getIntent().getStringExtra("nick");
        if (savedInstanceState == null) {
            onStartInfoLoading(new StartInfoLoadingEvent());
        }
        return view;
    }

    @Subscribe
    public void onStartInfoLoading(StartInfoLoadingEvent event) {
        BusProvider.getInstance().post(new ShowLoadingEvent(String.format(getString(R.string.info_loading), nick)));
        LoadTask loadTask = new LoadTask();
        loadTask.execute();
    }

    private class LoadTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            String[] values = new String[10];
            try {
                Statement statement = CurrentServer.getInstance().getGameSQL().createStatement();
                ResultSet rs = statement.executeQuery(
                        SQL.getCharacterInfo(nick, altCharInfo, enableCreateTime));
                if (rs.next()) {
                    charId = rs.getLong(SQL.charIdColumn);
                    clanId = rs.getLong(SQL.clanIdColumn);
                    if (enableCreateTime) {
                        if (altCreateTime) {
                            values[4] = rs.getDate(SQL.charCreateTimeColumn).toString();
                        } else
                            values[4] = getDate(-1, rs.getLong(SQL.charCreateTimeColumn));
                    }
                    values[0] = rs.getString(SQL.charLoginColumn);
                    values[1] = rs.getString(SQL.charLevelColumn);
                    values[2] = rs.getString(SQL.charPvpColumn) + " / " + rs.getString(SQL.charPkColumn);
                    values[3] = rs.getString(SQL.charKarmaColumn);
                    values[5] = rs.getLong(SQL.charOnlineTimeColumn) / 3600 + getString(R.string.hours);
                    values[6] = rs.getString(SQL.clanNameColumn);
                    values[7] = rs.getInt(SQL.charSexColumn) == 0 ? getString(R.string.male) : getString(R.string.female);
                    values[8] = L2Resources.getProfName(rs.getString(SQL.charClassIdColumn));
                    currentCP = rs.getInt(SQL.charCpColumn);
                    maxCP = rs.getInt(SQL.charMaxCpColumn);
                    currentHP = rs.getInt(SQL.charHpColumn);
                    maxHP = rs.getInt(SQL.charMaxHpColumn);
                    currentMP = rs.getInt(SQL.charMpColumn);
                    maxMP = rs.getInt(SQL.charMaxMpColumn);
                    lastAccess = rs.getLong(SQL.charLastActiveColumn);
                    status = rs.getInt(SQL.charOnlineColumn);
                    getActivity().getIntent().putExtra("status", status);
                }
                rs.close();
                rs = statement.executeQuery(SQL.getCharacterBaseProf(charId, altCharInfo));
                if (rs.next())
                    values[9] = L2Resources.getProfName(rs.getString(SQL.charIsBaseColumn));
                rs.close();
            } catch (SQLException e) {
                String fullError = "";
                String shortError = e.getMessage();
                for (StackTraceElement s : e.getStackTrace()) {
                    fullError += s.toString() + "\n";
                }
                BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                e.printStackTrace();
            }
            String[] keys = {getString(R.string.account), getString(R.string.level),
                    getString(R.string.pvp_pk), getString(R.string.karma),
                    getString(R.string.birthday), getString(R.string.total_online),
                    getString(R.string.clan), getString(R.string.gender),
                    getString(R.string.current_prof), getString(R.string.base_prof)};


            ArrayList<Map<String, Object>> data = new ArrayList<>(
                    values.length);
            Map<String, Object> m;
            for (int i = 0; i < values.length; i++) {
                m = new HashMap<>();
                m.put("value", values[i]);
                m.put("key", keys[i]);
                data.add(m);
            }

            String[] from = {"key", "value"};
            int[] to = {R.id.infoKeyTV, R.id.infoValueTV};

            sAdapter = new SimpleAdapter(getActivity().getApplicationContext(),
                    data, R.layout.info_row, from, to);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            fillInfo();
            BusProvider.getInstance().post(new StartInventoryLoadingEvent(charId, clanId));
            super.onPostExecute(aVoid);
        }
    }

    private void fillInfo() {
        cpPB.getProgressDrawable().setColorFilter(
                0xFFDDDD00, android.graphics.PorterDuff.Mode.SRC_IN);
        hpPB.getProgressDrawable().setColorFilter(
                Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
        mpPB.getProgressDrawable().setColorFilter(
                Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN);
        lastAccessTV.setText(getDate(status, lastAccess));
        if (status == 0) {
            statusTV.setTextColor(0xFFDD0000);
            statusTV.setText(R.string.offline);
        } else {
            statusTV.setTextColor(0xFF00DD00);
            statusTV.setText(R.string.online);
        }
        cpTV.setText(String.format(getString(R.string.slash_digitals), (int) currentCP, (int) maxCP));
        hpTV.setText(String.format(getString(R.string.slash_digitals), (int) currentHP, (int) maxHP));
        mpTV.setText(String.format(getString(R.string.slash_digitals), (int) currentMP, (int) maxMP));
        if (currentCP != 0)
            cpPB.setProgress((int) (currentCP / (maxCP / 100)));
        else
            cpPB.setProgress(0);
        if (currentCP != 0)
            hpPB.setProgress((int) (currentHP / (maxHP / 100)));
        else
            hpPB.setProgress(0);
        if (currentCP != 0)
            mpPB.setProgress((int) (currentMP / (maxMP / 100)));
        else
            mpPB.setProgress(0);
        infoGV.setAdapter(sAdapter);
    }

    private String getDate(int online, long date) {
        SimpleDateFormat full = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());
        SimpleDateFormat today = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Date resultdate;
        if (date < 10000000000L) {
            date *= 1000;
        }
        resultdate = new Date(date);

        if (online == 1) {
            return getString(R.string.enter_to_game) + " " + today.format(resultdate);
        } else if (online == 0) {
            return getString(R.string.was_in_game) + " " + full.format(resultdate);
        } else {
            return full.format(resultdate);
        }
    }


    @Override
    public void onResume() {
        BusProvider.getInstance().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
