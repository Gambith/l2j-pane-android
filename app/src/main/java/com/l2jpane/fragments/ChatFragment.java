package com.l2jpane.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.l2jpane.R;
import com.l2jpane.adapters.ChatAdapter;
import com.l2jpane.database.DBConnection;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.ShowErrorEvent;
import com.l2jpane.obj.ChatItem;
import com.l2jpane.singletone.CurrentServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class ChatFragment extends Fragment {


    private final ArrayList<ChatItem> chatList = new ArrayList<>();
    private boolean isFirstCommand = true;
    private String path = "";
    private String commandToRun = "";
    private String lastLine = "";
    private Timer timer;
    private Session session;
    private RecyclerView chatRV;
    private ChatAdapter adapter;
    private LinearLayoutManager llm;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        chatRV = (RecyclerView) view.findViewById(R.id.chatRV);
        llm = new LinearLayoutManager(getContext());
        llm.setStackFromEnd(true);
        chatRV.setLayoutManager(llm);
        if (adapter != null) {
            chatRV.setAdapter(adapter);
            Log.d("chat", "onCreateView: " + adapter.getItemCount());
        }
        if (isFirstCommand) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle(getString(R.string.please_wait));
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.loading_chat));
            progressDialog.show();
        }
        if (savedInstanceState == null) {
            if (CurrentServer.getInstance().getSelectedServerCFG().containsKey("gs_pathChat"))
                path = CurrentServer.getInstance().getSelectedServerCFG().get("gs_pathChat").toString();
            commandToRun = String.format("cat %s \n", path);
            adapter = new ChatAdapter(chatList);
            chatRV.setAdapter(adapter);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        session = DBConnection.getSSHSession();
                    } catch (JSchException e) {
                        String fullError = "";
                        String shortError = e.getMessage();
                        for (StackTraceElement s : e.getStackTrace()) {
                            fullError += s.toString() + "\n";
                        }
                        BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                        e.printStackTrace();
                    }
                }
            }).start();
            timer = new Timer(true);
            timer.schedule(new ReadTask(), 1000, 1000);
        }
        return view;
    }


    class ReadTask extends TimerTask {

        @Override
        public void run() {
            if (session != null && getActivity() != null) {
                try {
                    ChannelExec channel = (ChannelExec) session.openChannel("exec");
                    BufferedReader in = new BufferedReader(new InputStreamReader(channel.getInputStream(), "UTF8"));
                    channel.setCommand(commandToRun);
                    channel.connect();

                    String msg;
                    String msg1 = null;

                    while ((msg = in.readLine()) != null && !msg.equals("")) {
                        if (msg.equals(lastLine) || isFirstCommand) {
                            while ((msg = in.readLine()) != null && msg.length() > 0) {
                                msg1 = msg;
                                int nondateIndex = msg1.indexOf(']');
                                int[] nickIndecies = new int[2];
                                nickIndecies[0] = msg1.indexOf('[', nondateIndex + 1) + 1;
                                nickIndecies[1] = msg1.indexOf(']', nondateIndex + 1);
                                int textIndex = nickIndecies[1] + 2;
                                ChatItem item = new ChatItem();

                                if (msg1.substring(nondateIndex + 2).startsWith("TELL ")) {
                                    item.setType("PRIVATE");
                                } else if (msg1.substring(nondateIndex + 2).startsWith("ALL ")) {
                                    item.setType("ALL");
                                } else if (msg1.substring(nondateIndex + 2).startsWith("CLAN ")) {
                                    item.setType("CLAN");
                                } else if (msg1.substring(nondateIndex + 2).startsWith("PRIV_MSG           ")) {
                                    item.setType("PRIVATE CHAT");
                                } else if (msg1.substring(nondateIndex + 2).startsWith("ALL ")) {
                                    item.setType("ALL");
                                } else if (msg1.substring(nondateIndex + 2).startsWith("PARTY ")) {
                                    item.setType("PARTY");
                                } else if (msg1.substring(nondateIndex + 2).startsWith("TRADE ")) {
                                    item.setType("TRADE");
                                } else if (msg1.substring(nondateIndex + 2).startsWith("HERO_VOICE ")) {
                                    item.setType("HERO_VOICE");
                                } else if (msg1.substring(nondateIndex + 2).startsWith("SHOUT ")) {
                                    item.setType("SHOUT");
                                } else {
                                    item.setType("Unknown");
                                }
                                item.setDate(msg1.substring(msg1.indexOf('[') + 1, nondateIndex));
                                item.setNick(msg1.substring(nickIndecies[0], nickIndecies[1]));
                                if (!msg1.contains("Title=")) {
                                    item.setText(msg1.substring(textIndex));
                                } else {
                                    String formatMsg = msg1.substring(textIndex);
                                    formatMsg = formatMsg.replaceAll("", " ").replaceAll("\\u0009", " ");
                                    item.setText(formatMsg.replaceAll("(\\w.*?\\w=.*?)", " ").replaceAll("\\*  ", "\\*"));
                                }
                                chatList.add(item);
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            adapter.notifyDataSetChanged();
                                            if (llm.findLastVisibleItemPosition() > chatList.size() - 5)
                                                llm.scrollToPosition(chatList.size() - 1);
                                            Log.d("scroll", (llm.findLastVisibleItemPosition() > chatList.size() - 5) + "");
                                        }
                                    });
                                } else
                                    timer.cancel();
                            }
                        }
                    }
                    if (msg1 != null) {
                        lastLine = msg1;
                    }
                    if (isFirstCommand) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                llm.scrollToPosition(chatList.size() - 1);
                                progressDialog.dismiss();
                            }
                        });
                        commandToRun = "tail -20 " + path + " \n";
                        isFirstCommand = false;
                        if (chatList.isEmpty()) {
                            timer.cancel();
                            Looper.prepare();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), R.string.chat_log_null, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                    channel.disconnect();
                } catch (JSchException | IOException e) {
                    timer.cancel();
                    String fullError = "";
                    String shortError = e.getMessage();
                    for (StackTraceElement s : e.getStackTrace()) {
                        fullError += s.toString() + "\n";
                    }
                    BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                    e.printStackTrace();
                }
            }
        }
    }

}
