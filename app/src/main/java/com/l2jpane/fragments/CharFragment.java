package com.l2jpane.fragments;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.l2jpane.App;
import com.l2jpane.R;
import com.l2jpane.SlidingTabLayout;
import com.l2jpane.adapters.ViewPagerAdapter;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.FinishInventoryLoadingEvent;
import com.l2jpane.eventbus.ShowLoadingEvent;
import com.l2jpane.eventbus.StartInfoLoadingEvent;
import com.squareup.otto.Subscribe;


public class CharFragment extends Fragment {

    private ProgressDialog loadingDialog;
    private Toolbar toolbar;
    private CharSequence Titles[] = {App.getContext().getString(R.string.information), App.getContext().getString(R.string.inventory)};
    private String nick;
    private boolean isFinishLoading;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_char, container, false);
        loadingDialog = new ProgressDialog(view.getContext());
        nick = getActivity().getIntent().getStringExtra("nick");
        toolbar = (Toolbar) view.findViewById(R.id.tool_bar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);


        int numboftabs = 2;
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), Titles, numboftabs);
        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(adapter);
        SlidingTabLayout tabs = (SlidingTabLayout) view.findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return ContextCompat.getColor(App.getContext(), R.color.colorBlack);
            }
        });

        tabs.setViewPager(pager);
        setHasOptionsMenu(true);

        if (savedInstanceState == null || !isFinishLoading) {
            loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loadingDialog.setTitle(getString(R.string.please_wait));
            loadingDialog.setCancelable(false);
            BusProvider.getInstance().post(new StartInfoLoadingEvent());
        }
        return view;
    }

    @Subscribe
    public void showProgressDialog(ShowLoadingEvent event) {
        loadingDialog.setMessage(event.getMessage());
        if (!loadingDialog.isShowing())
            loadingDialog.show();
    }


    @Subscribe
    public void OnFinishInventoryLoading(FinishInventoryLoadingEvent event) {
        loadingDialog.dismiss();
        isFinishLoading = true;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_char_info, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.copyNick:
                ClipboardManager clipboard =
                        (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip =
                        ClipData.newPlainText("label", nick);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getActivity(), R.string.copied, Toast.LENGTH_SHORT).show();
                break;
            case R.id.reload:
                BusProvider.getInstance().post(new ShowLoadingEvent(String.format(getString(R.string.inventory_loading), nick)));
                BusProvider.getInstance().post(new StartInfoLoadingEvent());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        toolbar.setTitle(nick);
        toolbar.setNavigationIcon(R.drawable.ic_account_box_black_24dp);
        BusProvider.getInstance().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }
    @Override
    public void onStop() {
        super.onStop();
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }
}
