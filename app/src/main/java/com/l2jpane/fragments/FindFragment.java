package com.l2jpane.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.l2jpane.R;
import com.l2jpane.adapters.FindAccAdapter;
import com.l2jpane.adapters.FindCharAdapter;
import com.l2jpane.adapters.FindHwidAdapter;
import com.l2jpane.database.SQL;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.ShowErrorEvent;
import com.l2jpane.obj.SearchResultItem;
import com.l2jpane.singletone.CurrentServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class
FindFragment extends Fragment implements ListView.OnItemClickListener {
    private final List<RadioButton> ffGroup = new ArrayList<>();
    private final List<RadioButton> sfGroup = new ArrayList<>();
    private Button searchBtn;
    private RelativeLayout ffLayout;
    private RelativeLayout sfLayout;
    private RelativeLayout findLayout;
    private ProgressBar findProgress;
    private Snackbar searchSb;
    private EditText searchField;
    private RadioButton ffAcc;
    private RadioButton ffChar;
    private RadioButton ffHwid;
    private RadioButton sfLogin;
    private RadioButton sfNick;
    private RadioButton sfIP;
    private RadioButton sfHwid;
    private RadioButton sfEmail;
    private ListView searchLV;
    private FindAccAdapter findAccAdapter;
    private FindCharAdapter findCharAdapter;
    private FindHwidAdapter findHwidAdapter;
    private ArrayList<SearchResultItem> searchResults = new ArrayList<>();
    private FindTask fTask = new FindTask();
    private boolean enableEmail = false;
    private boolean enableHWID = false;
    private boolean altCharInfo = false;
    private boolean altAccessLevel = false;
    private static final String accPrefix = "ACC:";
    private int mPreviousVisibleItem;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        altCharInfo = CurrentServer.getInstance().getCurrentBuild().get("altCharInfo").equals("true");
        altAccessLevel = (CurrentServer.getInstance().getCurrentBuild().containsKey("altAccessLevel") &&
                CurrentServer.getInstance().getCurrentBuild().get("altAccessLevel").equals("true"));
        if (CurrentServer.getInstance().getSelectedServerCFG().containsKey("gs_HWID_log") &&
                CurrentServer.getInstance().getSelectedServerCFG().get("gs_HWID_log").toString().equals("true"))
            enableHWID = true;
        if (CurrentServer.getInstance().getSelectedServerCFG().containsKey("ls_email") &&
                CurrentServer.getInstance().getSelectedServerCFG().get("ls_email").toString().equals("true"))
            enableEmail = true;

        final View view = inflater.inflate(R.layout.fragment_find, container, false);
        ffLayout = (RelativeLayout) view.findViewById(R.id.firstFindLayout);
        findLayout = (RelativeLayout) view.findViewById(R.id.findLayout);
        sfLayout = (RelativeLayout) view.findViewById(R.id.secondFindLayout);
        searchBtn = (Button) view.findViewById(R.id.findBtn);
        findProgress = (ProgressBar) view.findViewById(R.id.findProgress);
        searchField = (EditText) view.findViewById(R.id.sfField);
        ffAcc = (RadioButton) view.findViewById(R.id.ffAcc);
        ffChar = (RadioButton) view.findViewById(R.id.ffChar);
        ffHwid = (RadioButton) view.findViewById(R.id.ffHwid);
        sfLogin = (RadioButton) view.findViewById(R.id.sfLogin);
        sfNick = (RadioButton) view.findViewById(R.id.sfNick);
        sfIP = (RadioButton) view.findViewById(R.id.sfIP);
        sfHwid = (RadioButton) view.findViewById(R.id.sfHwid);
        sfEmail = (RadioButton) view.findViewById(R.id.sfEmail);
        searchLV = (ListView) view.findViewById(R.id.searchList);
        searchLV.setOnItemClickListener(this);
        // Запрет на символ ', чотбы не дробить sql запрос
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                String blockCharacterSet = "'";
                if (source != null && blockCharacterSet.contains(("" + source))) {
                    return "";
                }
                return null;
            }
        };
        searchField.setFilters(new InputFilter[]{filter});
        findProgress.setVisibility(View.INVISIBLE);
        searchBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (findProgress.getVisibility() == ProgressBar.VISIBLE) {
                    stopSearch();
                }
                if (searchField.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), R.string.fillsearchfield, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (searchBtn.getText().toString().equals(getString(R.string.startsearch))) {
                    searchLV.setOnScrollListener(null);
                    if (!isFreeThread())
                        return;

                    searchField.clearFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(searchLV.getWindowToken(), 0);
                    findLayout.animate().translationY(-sfLayout.getHeight() - ffLayout.getHeight())
                            .setDuration(1000).setInterpolator(new AccelerateInterpolator(2)).start();

                    findLayout.getLayoutParams().height = findLayout.getHeight()
                            + ffLayout.getHeight() + sfLayout.getHeight() + findProgress.getHeight();

                    if (ffAcc.isChecked()) {
                        findAccAdapter = new FindAccAdapter(getContext(), searchResults);
                        searchLV.setAdapter(findAccAdapter);
                    } else if (ffChar.isChecked()) {
                        findCharAdapter = new FindCharAdapter(getContext(), searchResults);
                        searchLV.setAdapter(findCharAdapter);
                    } else if (ffHwid.isChecked()) {
                        findHwidAdapter = new FindHwidAdapter(getContext(), searchResults);
                        searchLV.setAdapter(findHwidAdapter);
                    }
                    searchLV.animate().alpha(1).setDuration(1000).setInterpolator(new AccelerateInterpolator(2)).start();
                    ffLayout.animate().alpha(.1F).setDuration(1000).setInterpolator(new AccelerateInterpolator(2)).start();
                    sfLayout.animate().alpha(.1F).setDuration(1000).setInterpolator(new AccelerateInterpolator(2)).start();
                    searchBtn.setText(R.string.cancelsearch);
                    findProgress.setVisibility(View.VISIBLE);
                    searchSb = Snackbar.make(v, getString(R.string.searching) + searchField.getText().toString(), Snackbar.LENGTH_INDEFINITE);
                    startSearch();

                } else {
                    findLayout.animate().translationY(0)
                            .setDuration(500).setInterpolator(new AccelerateInterpolator(1)).start();
                    searchBtn.animate().alpha(1F).setDuration(1000).setInterpolator(new AccelerateInterpolator(5)).start();
                    ffLayout.animate().alpha(1F).setDuration(1000).setInterpolator(new AccelerateInterpolator(5)).start();
                    sfLayout.animate().alpha(1F).setDuration(1000).setInterpolator(new AccelerateInterpolator(5)).start();
                    searchBtn.setText(R.string.startsearch);
                    searchLV.animate().alpha(0).setDuration(1000).setInterpolator(new AccelerateInterpolator(2)).start();
                    findLayout.getLayoutParams().height = findLayout.getHeight()
                            - ffLayout.getHeight() - sfLayout.getHeight() - findProgress.getHeight();
                    searchResults.clear();
                    searchResults = new ArrayList<>();
                    searchSb.dismiss();
                    findProgress.setVisibility(View.INVISIBLE);
                }
            }
        });

        ffGroup.add((RadioButton) view.findViewById(R.id.ffAcc));
        ffGroup.add((RadioButton) view.findViewById(R.id.ffChar));
        ffGroup.add((RadioButton) view.findViewById(R.id.ffHwid));
        createRadioList(ffGroup);

        sfGroup.add((RadioButton) view.findViewById(R.id.sfLogin));
        sfGroup.add((RadioButton) view.findViewById(R.id.sfNick));
        sfGroup.add((RadioButton) view.findViewById(R.id.sfIP));
        sfGroup.add((RadioButton) view.findViewById(R.id.sfHwid));
        sfGroup.add((RadioButton) view.findViewById(R.id.sfEmail));
        createRadioList(sfGroup);


        return view;
    }

    private boolean isFreeThread() {
        if (fTask.getStatus() == AsyncTask.Status.FINISHED || fTask.getStatus() == AsyncTask.Status.PENDING) {
            fTask = new FindTask();
            searchResults = new ArrayList<>();
            return true;
        } else {
            Toast.makeText(getActivity(), R.string.threadisrunning, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void stopSearch() {
        fTask.cancel(false);
    }

    private void startSearch() {
        fTask.execute(searchField.getText().toString());
    }

    private class FindTask extends AsyncTask<String, SearchResultItem, Void> {
        int findMethod = -1;
        Statement loginSt;
        Statement gameSt;

        @Override
        protected void onPreExecute() {
            searchSb.show();

            if (ffAcc.isChecked()) {
                if (sfLogin.isChecked()) {
                    findMethod = 1;
                } else if (sfNick.isChecked()) {
                    findMethod = 2;
                } else if (sfIP.isChecked()) {
                    findMethod = 3;
                } else if (sfEmail.isChecked()) {
                    findMethod = 4;
                } else if (sfHwid.isChecked()) {
                    findMethod = 5;
                }
            } else if (ffChar.isChecked()) {
                if (sfLogin.isChecked()) {
                    findMethod = 6;
                } else if (sfNick.isChecked()) {
                    findMethod = 7;
                }
            } else if (ffHwid.isChecked()) {
                if (sfLogin.isChecked()) {
                    findMethod = 8;
                } else if (sfNick.isChecked()) {
                    findMethod = 9;
                }
            }
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(SearchResultItem... values) {
            super.onProgressUpdate(values);
            searchResults.add(values[0]);
            if (ffAcc.isChecked())
                findAccAdapter.notifyDataSetChanged();
            else if (ffChar.isChecked())
                findCharAdapter.notifyDataSetChanged();
            else if (ffHwid.isChecked())
                findHwidAdapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(String... params) {
            SystemClock.sleep(1000);
            try {
                loginSt = CurrentServer.getInstance().getLoginSQL().createStatement();
                gameSt = CurrentServer.getInstance().getGameSQL().createStatement();
                ResultSet rs;

                if (params[0].startsWith(accPrefix)) {
                    findMethod = 10;
                    params[0] = params[0].substring(accPrefix.length());
                }
                switch (findMethod) {
                    case 1:
                        rs = loginSt.executeQuery(SQL.getAccountByLogin(params[0], enableEmail));
                        while (rs.next()) {

                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.accLoginColumn));
                            searchResultItem.setIp(rs.getString(SQL.accLastIpColumn));
                            searchResultItem.setAccessLevel(rs.getInt(SQL.accAccessLevelColumn));
                            searchResultItem.setLastAccess(rs.getLong(SQL.accLastActiveColumn));
                            if (enableEmail)
                                searchResultItem.setEmail(rs.getString(SQL.accEmailColumn));

                            if (enableHWID) {
                                ResultSet rs1 = gameSt.executeQuery(SQL.getLastHWID(searchResultItem.getLogin()));
                                if (rs1.next())
                                    searchResultItem.setHwid(rs1.getString("hwid"));
                            }

                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                        }
                        rs.close();
                        break;
                    case 2:
                        String login = "";
                        rs = gameSt.executeQuery(SQL.getLoginByNick(params[0]));
                        if (rs.next())
                            login = rs.getString(SQL.charLoginColumn);
                        rs.close();
                        rs = loginSt.executeQuery(SQL.getOneAccountByLogin(login, enableEmail));
                        while (rs.next()) {

                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.accLoginColumn));
                            searchResultItem.setIp(rs.getString(SQL.accLastIpColumn));
                            searchResultItem.setAccessLevel(rs.getInt(SQL.accAccessLevelColumn));
                            searchResultItem.setLastAccess(rs.getLong(SQL.accLastActiveColumn));
                            if (enableEmail)
                                searchResultItem.setEmail(rs.getString(SQL.accEmailColumn));

                            if (enableHWID) {
                                ResultSet rs1 = gameSt.executeQuery(SQL.getLastHWID(searchResultItem.getLogin()));
                                if (rs1.next())
                                    searchResultItem.setHwid(rs1.getString("hwid"));
                            }

                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                        }
                        rs.close();
                        break;
                    case 3:
                        rs = loginSt.executeQuery(SQL.getAccountByIP(params[0], enableEmail));
                        while (rs.next()) {

                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.accLoginColumn));
                            searchResultItem.setIp(rs.getString(SQL.accLastIpColumn));
                            searchResultItem.setAccessLevel(rs.getInt(SQL.accAccessLevelColumn));
                            searchResultItem.setLastAccess(rs.getLong(SQL.accLastActiveColumn));
                            if (enableEmail)
                                searchResultItem.setEmail(rs.getString(SQL.accEmailColumn));

                            if (enableHWID) {
                                ResultSet rs1 = gameSt.executeQuery(SQL.getLastHWID(searchResultItem.getLogin()));
                                if (rs1.next())
                                    searchResultItem.setHwid(rs1.getString("hwid"));
                            }

                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                        }
                        rs.close();
                        break;
                    case 4:
                        rs = loginSt.executeQuery(SQL.getAccountByEmail(params[0]));
                        while (rs.next()) {

                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.accLoginColumn));
                            searchResultItem.setIp(rs.getString(SQL.accLastIpColumn));
                            searchResultItem.setAccessLevel(rs.getInt(SQL.accAccessLevelColumn));
                            searchResultItem.setLastAccess(rs.getLong(SQL.accLastActiveColumn));
                            searchResultItem.setEmail(rs.getString(SQL.accEmailColumn));

                            if (enableHWID) {
                                ResultSet rs1 = gameSt.executeQuery(SQL.getLastHWID(searchResultItem.getLogin()));
                                if (rs1.next())
                                    searchResultItem.setHwid(rs1.getString("hwid"));
                            }

                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                        }
                        rs.close();
                        break;
                    case 5:
                        ArrayList<String> accountsOfHWID = new ArrayList<>();
                        rs = gameSt.executeQuery(SQL.getLoginsByHWID(params[0]));
                        while (rs.next()) {
                            if (!accountsOfHWID.contains(rs.getString("account")))
                                accountsOfHWID.add(rs.getString("account"));
                        }
                        rs.close();
                        for (String acc : accountsOfHWID) {
                            rs = loginSt.executeQuery(SQL.getAccountByLogin(acc, enableEmail));
                            while (rs.next()) {

                                SearchResultItem searchResultItem = new SearchResultItem();
                                searchResultItem.setLogin(rs.getString(SQL.accLoginColumn));
                                searchResultItem.setIp(rs.getString(SQL.accLastIpColumn));
                                searchResultItem.setAccessLevel(rs.getInt(SQL.accAccessLevelColumn));
                                searchResultItem.setLastAccess(rs.getLong(SQL.accLastActiveColumn));
                                if (enableEmail)
                                    searchResultItem.setEmail(rs.getString(SQL.accEmailColumn));
                                ResultSet rs1 = gameSt.executeQuery(SQL.getLastHWID(searchResultItem.getLogin()));
                                if (rs1.next())
                                    searchResultItem.setHwid(rs1.getString("hwid"));
                                rs1.close();
                                if (fTask.isCancelled()) {
                                    cancel(false);
                                    return null;
                                }
                                publishProgress(searchResultItem);
                                SystemClock.sleep(50);
                            }
                            rs.close();
                        }
                        break;
                    case 6:
                        rs = gameSt.executeQuery(SQL.getCharByLogin(params[0], altCharInfo));
                        while (rs.next()) {
                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.charLoginColumn));
                            searchResultItem.setNick(rs.getString(SQL.charNickColumn));
                            searchResultItem.setStatus(rs.getString(SQL.charOnlineColumn));
                            searchResultItem.setProf(rs.getString(SQL.charClassIdColumn));
                            searchResultItem.setLevel(rs.getString(SQL.charLevelColumn));
                            searchResultItem.setAccessLevel(rs.getInt(SQL.charAccessLevelColumn));
                            searchResultItem.setLastAccess(rs.getLong(SQL.charLastActiveColumn));
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                        }
                        rs.close();
                        break;
                    case 7:
                        rs = gameSt.executeQuery(SQL.getCharByNick(params[0], altCharInfo));

                        while (rs.next()) {
                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.charLoginColumn));
                            searchResultItem.setNick(rs.getString(SQL.charNickColumn));
                            searchResultItem.setStatus(rs.getString(SQL.charOnlineColumn));
                            searchResultItem.setProf(rs.getString(SQL.charClassIdColumn));
                            searchResultItem.setLevel(rs.getString(SQL.charLevelColumn));
                            searchResultItem.setAccessLevel(rs.getInt(SQL.charAccessLevelColumn));
                            searchResultItem.setLastAccess(rs.getLong(SQL.charLastActiveColumn));
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                        }
                        rs.close();
                        break;
                    case 8:
                        rs = gameSt.executeQuery(SQL.getHwidByLogin(params[0]));
                        while (rs.next()) {

                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.hwidLoginColumn));
                            searchResultItem.setLastAccess(rs.getString(SQL.hwidDateColumn));
                            searchResultItem.setHwid(rs.getString(SQL.hwidHwidColumn));

                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                        }
                        rs.close();
                        break;
                    case 9:
                        rs = gameSt.executeQuery(SQL.getHwidByNick(params[0]));
                        while (rs.next()) {

                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.hwidLoginColumn));
                            searchResultItem.setLastAccess(rs.getString(SQL.hwidDateColumn));
                            searchResultItem.setHwid(rs.getString(SQL.hwidHwidColumn));

                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                        }
                        rs.close();
                        break;
                    case 10:
                        rs = gameSt.executeQuery(SQL.getCharactersOnAcc(params[0], altCharInfo));
                        while (rs.next()) {
                            SearchResultItem searchResultItem = new SearchResultItem();
                            searchResultItem.setLogin(rs.getString(SQL.charLoginColumn));
                            searchResultItem.setNick(rs.getString(SQL.charNickColumn));
                            searchResultItem.setStatus(rs.getString(SQL.charOnlineColumn));
                            searchResultItem.setProf(rs.getString(SQL.charClassIdColumn));
                            searchResultItem.setLevel(rs.getString(SQL.charLevelColumn));
                            searchResultItem.setAccessLevel(rs.getInt(SQL.charAccessLevelColumn));
                            searchResultItem.setLastAccess(rs.getLong(SQL.charLastActiveColumn));
                            publishProgress(searchResultItem);
                            SystemClock.sleep(50);
                            if (fTask.isCancelled()) {
                                cancel(false);
                                return null;
                            }
                        }
                        rs.close();
                        break;
                }
            } catch (SQLException e) {
                String fullError = "";
                String shortError = e.getMessage();
                for (StackTraceElement s : e.getStackTrace()) {
                    fullError += s.toString() + "\n";
                }
                BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                e.printStackTrace();
            }
            if (fTask.isCancelled()) {
                cancel(false);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!isCancelled()) {
                searchBtn.setText(R.string.newsearch);
                findLayout.animate().translationY(-ffLayout.getHeight() - findProgress.getHeight() - sfLayout.getHeight())
                        .setDuration(400).setInterpolator(new AccelerateInterpolator(2)).start();

                findProgress.setVisibility(View.INVISIBLE);
                if (searchLV.getAdapter().getCount() == 0)
                    searchSb.setText(R.string.nothingfound);
                else {
                    searchSb.setText(getString(R.string.found) + " "
                            + searchLV.getAdapter().getCount());
                    searchLV.setOnScrollListener(new AbsListView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {

                        }

                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            if (firstVisibleItem > mPreviousVisibleItem) {
                                if (searchSb != null && searchSb.isShown())
                                    searchSb.dismiss();
                            } else if (firstVisibleItem < mPreviousVisibleItem) {
                                if (searchSb != null && !searchSb.isShown())
                                    searchSb.show();
                            }
                            mPreviousVisibleItem = firstVisibleItem;
                        }
                    });
                }
            }
        }

        @Override
        protected void onCancelled() {
            try {
                if (loginSt != null && !loginSt.isClosed())
                    loginSt.close();
                if (gameSt != null && !gameSt.isClosed())
                    gameSt.close();
            } catch (SQLException e) {
                String fullError = "";
                String shortError = e.getMessage();
                for (StackTraceElement s : e.getStackTrace()) {
                    fullError += s.toString() + "\n";
                }
                BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                e.printStackTrace();
            }
            super.onCancelled();
        }
    }

    private void createRadioList(final List<RadioButton> radioButtons) {
        for (RadioButton button : radioButtons) {

            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) processRadioButtonClick(buttonView);
                }

                private void processRadioButtonClick(CompoundButton buttonView) {

                    for (RadioButton button : radioButtons) {

                        if (button != buttonView) button.setChecked(false);
                    }
                    if (buttonView.equals(ffAcc)) {
                        if (enableHWID)
                            sfHwid.setEnabled(true);

                        sfIP.setEnabled(true);
                        if (enableEmail)
                            sfEmail.setEnabled(true);

                    } else if (buttonView.equals(ffChar) || buttonView.equals(ffHwid)) {
                        sfEmail.setEnabled(false);
                        sfIP.setEnabled(false);
                        sfHwid.setEnabled(false);
                        for (RadioButton button : sfGroup) {
                            if (button != sfLogin) button.setChecked(false);
                            else button.setChecked(true);
                        }
                    }
                    if (!enableEmail)
                        sfEmail.setEnabled(false);

                    if (sfLogin.isChecked())
                        searchField.setHint("Введите часть имени аккаунта");
                    else if (sfNick.isChecked() && ffAcc.isChecked())
                        searchField.setHint("Введите ник персонажа полностью");
                    else if (sfNick.isChecked())
                        searchField.setHint("Введите часть имени персонажа");
                    else if (sfIP.isChecked())
                        searchField.setHint("Введите часть IP адреса");
                    else if (sfEmail.isChecked())
                        searchField.setHint("Введите часть почты аккаунта");
                    else if (sfHwid.isChecked())
                        searchField.setHint("Введите часть HWID");
                }
            });
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        registerForContextMenu(searchLV);
        view.showContextMenu();
        unregisterForContextMenu(searchLV);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        if (searchLV.getAdapter().equals(findAccAdapter))
            inflater.inflate(R.menu.menu_acc_row, menu);
        else if (searchLV.getAdapter().equals(findCharAdapter))
            inflater.inflate(R.menu.menu_char_row, menu);
        else
            inflater.inflate(R.menu.menu_hwid_row, menu);


        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        int itemID = info.position;
        if (searchLV.getAdapter().equals(findAccAdapter)) {
            menu.setHeaderTitle(searchResults.get(itemID).getLogin());
            if (!enableEmail) {
                menu.findItem(R.id.copyEmail).setVisible(false);
                menu.findItem(R.id.editEmail).setVisible(false);
            }
            if (!enableHWID) {
                menu.findItem(R.id.copyHwid).setVisible(false);
            }
            if (searchResults.get(itemID).getAccessLevel() < 0)
                menu.findItem(R.id.banAcc).setVisible(false);
            else
                menu.findItem(R.id.unbanAcc).setVisible(false);

        } else if (searchLV.getAdapter().equals(findCharAdapter)) {
            menu.setHeaderTitle(searchResults.get(itemID).getNick());
            if (searchResults.get(itemID).getAccessLevel() < 0)
                menu.findItem(R.id.banChar).setVisible(false);
            else
                menu.findItem(R.id.unbanChar).setVisible(false);
        } else if (searchLV.getAdapter().equals(findHwidAdapter)) {
            menu.setHeaderTitle(searchResults.get(itemID).getLogin());
        }
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final AlertDialog ad;
        final EditText et = new EditText(getActivity());
        et.setSelectAllOnFocus(true);
        et.setSingleLine(true);
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final String nick;
        final String login;
        final SearchResultItem current = searchResults.get(info.position);
        String toCopy = null;
        try {
            final Statement loginSt = CurrentServer.getInstance().getLoginSQL().createStatement();
            final Statement gameSt = CurrentServer.getInstance().getGameSQL().createStatement();
            switch (item.getItemId()) {
                case R.id.copyLogin:
                    toCopy = current.getLogin();
                    break;
                case R.id.copyNick:
                    toCopy = current.getNick();
                    break;
                case R.id.copyIp:
                    toCopy = current.getIp();
                    break;
                case R.id.copyEmail:
                    toCopy = current.getEmail();
                    break;
                case R.id.copyHwid:
                    toCopy = current.getHwid();
                    break;
                case R.id.banAcc:
                    login = current.getLogin();
                    new UpdateItemTask().execute(String.valueOf(item.getItemId()), login);
                    current.setAccessLevel(-100);
                    findAccAdapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "Аккаунт " + login + " заблокирован", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.unbanAcc:
                    login = current.getLogin();
                    new UpdateItemTask().execute(String.valueOf(item.getItemId()), login);
                    current.setAccessLevel(0);
                    findAccAdapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "Аккаунт " + login + " разблокирован", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.banChar:
                    nick = current.getNick();
                    new UpdateItemTask().execute(String.valueOf(item.getItemId()), nick, String.valueOf(altAccessLevel));
                    current.setAccessLevel(-100);
                    findCharAdapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "Персонаж " + nick + " заблокирован", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.unbanChar:
                    nick = current.getNick();
                    new UpdateItemTask().execute(String.valueOf(item.getItemId()), nick, String.valueOf(altAccessLevel));
                    current.setAccessLevel(0);
                    findCharAdapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "Персонаж " + nick + " разблокирован", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.editEmail:
                    et.setText(current.getEmail());
                    builder.setView(et)
                            .setTitle(getString(R.string.edit_email))
                            .setIcon(R.drawable.ic_border_color_black_24dp)
                            .setPositiveButton(getString(R.string.change), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    final String newEmail = et.getText().toString();
                                    current.setEmail(newEmail);
                                    findAccAdapter.notifyDataSetChanged();
                                    new UpdateItemTask().execute(String.valueOf(item.getItemId()), newEmail, current.getLogin());
                                    Toast.makeText(getActivity(), R.string.email_changed, Toast.LENGTH_SHORT).show();
                                }
                            });
                    ad = builder.create();
                    ad.show();
                    break;
                case R.id.editLogin:
                    et.setText(current.getLogin());
                    builder.setView(et)
                            .setTitle(getString(R.string.edit_login))
                            .setIcon(R.drawable.ic_border_color_black_24dp)
                            .setPositiveButton(getString(R.string.change), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    final String newLogin = et.getText().toString();
                                    current.setLogin(newLogin);
                                    findCharAdapter.notifyDataSetChanged();
                                    new UpdateItemTask()
                                            .execute(String.valueOf(item.getItemId()), newLogin, current.getNick());
                                    Toast.makeText(getActivity(), R.string.login_changed, Toast.LENGTH_SHORT).show();
                                }
                            });
                    ad = builder.create();
                    ad.show();
                    break;
                case R.id.charsOnAcc:
                    if (!isFreeThread())
                        return true;
                    searchResults.clear();
                    findCharAdapter = new FindCharAdapter(getContext(), searchResults);
                    searchLV.setAdapter(findCharAdapter);
                    fTask.execute(accPrefix + current.getLogin());
                    searchSb.dismiss();
                    if (getActivity() != null && getActivity().getCurrentFocus() != null)
                        searchSb = Snackbar.make(getActivity().getCurrentFocus(), getString(R.string.chars_on_acc) + current.getLogin(), Snackbar.LENGTH_INDEFINITE);
                    searchSb.show();
                    break;
                case R.id.aboutChar:
                    fTask.cancel(false);
                    getActivity().getIntent().putExtra("nick", current.getNick());
                    CharFragment fragment2 = new CharFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.replace(R.id.frameContainer, fragment2, "CHAR_INFO");
                    fragmentTransaction.commit();
                    searchSb.dismiss();
                    break;

            }
            loginSt.close();
            gameSt.close();
            if (toCopy != null) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", toCopy);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getActivity(), R.string.copied, Toast.LENGTH_SHORT).show();
            }
        } catch (SQLException e) {
            String fullError = "";
            String shortError = e.getMessage();
            for (StackTraceElement s : e.getStackTrace()) {
                fullError += s.toString() + "\n";
            }
            BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
            e.printStackTrace();
        }
        return true;
    }


    private class UpdateItemTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            try {
                Statement loginSt = CurrentServer.getInstance().getLoginSQL().createStatement();
                Statement gameSt = CurrentServer.getInstance().getGameSQL().createStatement();
                switch (Integer.parseInt(params[0])) {
                    case R.id.banAcc:
                        loginSt.executeUpdate(SQL.banAcc(params[1]));
                        break;
                    case R.id.unbanAcc:
                        loginSt.executeUpdate(SQL.unbanAcc(params[1]));
                        break;
                    case R.id.banChar:
                        gameSt.executeUpdate(SQL.banChar(params[1], Boolean.parseBoolean(params[2])));
                        break;
                    case R.id.unbanChar:
                        gameSt.executeUpdate(SQL.unbanChar(params[1]));
                        break;
                    case R.id.editEmail:
                        loginSt.executeUpdate(SQL.changeEmail(params[1], params[2]));
                        break;
                    case R.id.editLogin:
                        loginSt.executeUpdate(SQL.changeLogin(params[1], params[2]));
                        break;
                }
            } catch (SQLException e) {
                String fullError = "";
                String shortError = e.getMessage();
                for (StackTraceElement s : e.getStackTrace()) {
                    fullError += s.toString() + "\n";
                }
                BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public void onDestroyView() {
        fTask.cancel(false);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (searchSb != null && searchSb.isShown())
            searchSb.dismiss();
        if (fTask != null)
            fTask.cancel(false);
        super.onDestroy();
    }
}
