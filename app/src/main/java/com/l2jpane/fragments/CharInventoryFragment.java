package com.l2jpane.fragments;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.l2jpane.L2Resources;
import com.l2jpane.R;
import com.l2jpane.adapters.InventoryAdapter;
import com.l2jpane.database.SQL;
import com.l2jpane.dialogs.AddItemDialog;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.FinishInventoryLoadingEvent;
import com.l2jpane.eventbus.ShowErrorEvent;
import com.l2jpane.eventbus.ShowLoadingEvent;
import com.l2jpane.eventbus.StartInventoryLoadingEvent;
import com.l2jpane.obj.InventoryItem;
import com.l2jpane.singletone.CurrentServer;
import com.squareup.otto.Subscribe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CharInventoryFragment extends Fragment implements ListView.OnItemClickListener{

    private ArrayList<InventoryItem> inventoryArrayList = new ArrayList<>();

    private final int UPDATE_COUNT = -1;
    private final int UPDATE_ENCHANT = -2;
    private final int DELETE_ITEM = -3;
    private ListView inventoryLV;
    private InventoryAdapter inventoryAdapter;
    private FloatingActionMenu addItemFAB;
    private boolean enableElements = false;
    private boolean altElements = false;
    private boolean delayWithoutEnchant = false;
    private boolean delayed = false;
    private long charId;
    private long clanId;
    private String nick;
    private int mPreviousVisibleItem;
    private FloatingActionButton addStackBtn;
    private FloatingActionButton addNonstackBtn;
    private final Bundle args = new Bundle();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);

        View view = inflater.inflate(R.layout.fragment_char_inventory, container, false);
        addItemFAB = (FloatingActionMenu) view.findViewById(R.id.addItemFAB);
        addStackBtn = (FloatingActionButton) view.findViewById(R.id.addStackBtn);
        addNonstackBtn = (FloatingActionButton) view.findViewById(R.id.addNonstackBtn);
        inventoryLV = (ListView) view.findViewById(R.id.inventoryLV);
        if (CurrentServer.getInstance().getCurrentBuild().containsKey("delayed"))
            delayed = CurrentServer.getInstance().getCurrentBuild().get("delayed").equals("true");
        if (CurrentServer.getInstance().getCurrentBuild().containsKey("delayWithoutEnchant"))
            delayWithoutEnchant = CurrentServer.getInstance().getCurrentBuild().get("delayWithoutEnchant").equals("true");
        if (CurrentServer.getInstance().getCurrentBuild().containsKey("enableElements"))
            enableElements = CurrentServer.getInstance().getCurrentBuild().get("enableElements").equals("true");
        if (CurrentServer.getInstance().getCurrentBuild().containsKey("altElements"))
            altElements = CurrentServer.getInstance().getCurrentBuild().get("altElements").equals("true");
        if (inventoryAdapter != null)
            inventoryLV.setAdapter(inventoryAdapter);
        args.putBoolean("delayed", delayed);
        args.putBoolean("delayWithoutEnchant", delayWithoutEnchant);
        args.putBoolean("enableElements", enableElements);
        inventoryLV.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        nick = getActivity().getIntent().getStringExtra("nick");
        inventoryLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > mPreviousVisibleItem) {
                    addItemFAB.hideMenuButton(true);
                } else if (firstVisibleItem < mPreviousVisibleItem) {
                    addItemFAB.showMenuButton(true);
                }
                mPreviousVisibleItem = firstVisibleItem;
            }
        });
        addStackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
                DialogFragment newFragment = new AddItemDialog();
                args.putBoolean("stackable", true);
                newFragment.setArguments(args);
                newFragment.show(ft, "AddItemDialog");
                addItemFAB.close(true);
            }
        });
        addNonstackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
                DialogFragment newFragment = new AddItemDialog();
                args.putBoolean("stackable", false);
                newFragment.setArguments(args);
                newFragment.show(ft, "AddItemDialog");
                addItemFAB.close(true);
            }
        });
    }

    @Subscribe
    public void OnStartInventoryLoading(StartInventoryLoadingEvent event) {
        if (!inventoryArrayList.isEmpty())
            inventoryArrayList.clear();
        BusProvider.getInstance().post(new ShowLoadingEvent(String.format(getString(R.string.inventory_loading), nick)));
        charId = event.getCharId();
        clanId = event.getClanId();
        LoadInventoryTask loadInventoryTask = new LoadInventoryTask();
        loadInventoryTask.execute();
        if(!delayed && getActivity().getIntent().getIntExtra("status", 0) == 1)
            addItemFAB.setOnMenuButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(), R.string.warning_l2j_online, Toast.LENGTH_SHORT).show();
                }
            });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        registerForContextMenu(inventoryLV);
        view.showContextMenu();
        unregisterForContextMenu(inventoryLV);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_inventory_row, menu);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        int position = info.position;
        menu.setHeaderTitle(inventoryArrayList.get(position).getItemName());
        if(!inventoryArrayList.get(position).getCount().isEmpty())
            menu.findItem(R.id.changeEnchant).setVisible(false);
        else if (inventoryArrayList.get(position).getEnchant() > 0 ||
                    !inventoryArrayList.get(position).getElemental().isEmpty())
            menu.findItem(R.id.changeCount).setVisible(false);
    }

    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final AlertDialog ad;
        final EditText et = new EditText(getActivity());
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final InventoryItem current = inventoryArrayList.get(info.position);

        final boolean delayedItem = current.getLocation().isEmpty();
        et.setInputType(InputType.TYPE_CLASS_NUMBER);
        et.setSelectAllOnFocus(true);
        et.setSingleLine(true);
        builder.setIcon(R.drawable.ic_border_color_black_24dp).setView(et);
        switch (item.getItemId()) {
            case R.id.changeCount:
                if(!current.getCount().isEmpty())
                    et.setText(current.getCount().substring(1));
                builder.setTitle(getString(R.string.change_count))
                        .setPositiveButton(getString(R.string.change), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                final String newCount = et.getText().toString();
                                current.setCount(Long.parseLong(newCount));
                                inventoryAdapter.notifyDataSetChanged();
                                new UpdateItemTask(UPDATE_COUNT, delayedItem).execute(String.valueOf(current.getObjItemId()),
                                        newCount, getString(R.string.count_changed));
                            }
                        });
                ad = builder.create();
                ad.show();
                break;
            case R.id.changeEnchant:
                et.setText(String.valueOf(current.getEnchant()));
                builder.setTitle(getString(R.string.change_enchant))
                        .setPositiveButton(getString(R.string.change), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                final String newEnchant = et.getText().toString();
                                current.setEnchant(Integer.parseInt(newEnchant));
                                inventoryAdapter.notifyDataSetChanged();
                                new UpdateItemTask(UPDATE_ENCHANT, delayedItem).execute(String.valueOf(current.getObjItemId()),
                                        newEnchant, getString(R.string.enchant_changed));
                            }
                        });
                ad = builder.create();
                ad.show();
                break;
            case R.id.deleteItem:
                inventoryArrayList.remove(current);
                inventoryAdapter.notifyDataSetChanged();
                new UpdateItemTask(DELETE_ITEM, delayedItem).execute(String.valueOf(current.getObjItemId()),
                        getString(R.string.item_removed));
                break;
        }
        return true;
    }

    private class LoadInventoryTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Statement gameSt = CurrentServer.getInstance().getGameSQL().createStatement();
                ResultSet rs = gameSt.executeQuery(SQL.getCharItems(charId, clanId, altElements));
                while (rs.next()) {
                    InventoryItem item = new InventoryItem();
                    item.setObjItemId(rs.getLong(SQL.itemsObjIdColumn));
                    item.setItemId(rs.getInt(SQL.itemsIdColumn));
                    item.setCount(rs.getLong(SQL.itemsCountColumn));
                    item.setLocation(rs.getString(SQL.itemsLocColumn));
                    item.setEnchant(rs.getInt(SQL.itemsEnchantColumn));
                    item.setItemName();
                    item.setItemIcon();
                    if (enableElements) {
                        if (item.getCount().isEmpty()) {
                            String itemElements = "";
                            if (altElements) {
                                if (rs.getInt(SQL.itemsElemFireColumn) > 0) {
                                    itemElements += getString(R.string.fire) +
                                            ": " + rs.getInt(SQL.itemsElemFireColumn) + " / ";
                                }
                                if (rs.getInt(SQL.itemsElemWaterColumn) > 0) {
                                    itemElements += getString(R.string.water) +
                                            ": " + rs.getInt(SQL.itemsElemWaterColumn) + " / ";
                                }
                                if (rs.getInt(SQL.itemsElemWindColumn) > 0) {
                                    itemElements += getString(R.string.wind) +
                                            ": " + rs.getInt(SQL.itemsElemWindColumn) + " / ";
                                }
                                if (rs.getInt(SQL.itemsElemEarthColumn) > 0) {
                                    itemElements += getString(R.string.earth) +
                                            ": " + rs.getInt(SQL.itemsElemEarthColumn) + " / ";
                                }
                                if (rs.getInt(SQL.itemsElemHolyColumn) > 0) {
                                    itemElements += getString(R.string.holy) +
                                            ": " + rs.getInt(SQL.itemsElemHolyColumn) + " / ";
                                }
                                if (rs.getInt(SQL.itemsElemUnholyColumn) > 0) {
                                    itemElements += getString(R.string.unholy) +
                                            ": " + rs.getInt(SQL.itemsElemUnholyColumn) + " / ";
                                }
                            } else {
                                Statement tempSt = CurrentServer.getInstance().getGameSQL().createStatement();
                                ResultSet tempRs = tempSt.executeQuery(SQL.getItemElement(item.getObjItemId()));
                                while (tempRs.next()) {
                                    String elemType = L2Resources.getElement(tempRs.getInt(SQL.elementalTypeColumn));
                                    itemElements += elemType + ": " + tempRs.getInt(SQL.elementalValueColumn) + " / ";
                                }
                            }
                            if (!itemElements.isEmpty()) {
                                item.setElemental(itemElements.substring(0, itemElements.length() - 3));
                            }
                        }
                    }
                    inventoryArrayList.add(item);
                }
                rs.close();
                if (delayed) {
                    rs = gameSt.executeQuery(SQL.getCharDelayedItems(charId));
                    while (rs.next()) {
                        InventoryItem item = new InventoryItem();
                        item.setObjItemId(rs.getLong(SQL.delayObjIdColumn));
                        item.setItemId(rs.getInt(SQL.delayIdColumn));
                        item.setCount(rs.getLong(SQL.delayCountColumn));
                        item.setEnchant(rs.getInt(SQL.delayEnchantColumn));
                        item.setLocation("DELAYED");
                        item.setItemName();
                        item.setItemIcon();
                        inventoryArrayList.add(item);
                    }
                    rs.close();
                }
                gameSt.close();
                inventoryAdapter = new InventoryAdapter(getContext(), inventoryArrayList);
            } catch (SQLException e) {
                String fullError = "";
                String shortError = e.getMessage();
                for (StackTraceElement s : e.getStackTrace()) {
                    fullError += s.toString() + "\n";
                }
                BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            args.putLong("charId", charId);
            args.putLong("clanId", clanId);
            inventoryLV.setAdapter(inventoryAdapter);
            BusProvider.getInstance().post(new FinishInventoryLoadingEvent());
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void onResume() {
        BusProvider.getInstance().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class UpdateItemTask extends AsyncTask<String, Void, String> {
        private int status;
        private boolean delayedItem;

        private UpdateItemTask(int _status, boolean _delayedItem) {
            status = _status;
            delayedItem = _delayedItem;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Statement st = CurrentServer.getInstance().getGameSQL().createStatement();
                switch (status) {
                    case UPDATE_COUNT:
                        st.executeUpdate(SQL.updateCount(delayedItem, params[0], params[1]));
                        break;
                    case UPDATE_ENCHANT:
                        st.executeUpdate(SQL.updateEnchant(delayedItem, params[0], params[1]));
                        break;
                    case DELETE_ITEM:
                        st.executeUpdate(SQL.deleteItem(delayedItem, params[0]));
                        return params[1];
                }
            } catch (SQLException e) {
                String fullError = "";
                String shortError = e.getMessage();
                for (StackTraceElement s : e.getStackTrace()) {
                    fullError += s.toString() + "\n";
                }
                BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                cancel(false);
                e.printStackTrace();
            }
            return params[2];
        }

        @Override
        protected void onPostExecute(String s) {
            if(!isCancelled())
                Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
        }
    }
}
