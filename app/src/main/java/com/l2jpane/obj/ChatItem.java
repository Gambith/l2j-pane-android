package com.l2jpane.obj;

/**
 * Created by aleksey on 23.06.16.
 */
public class ChatItem {
    private String type;
    private String date;
    private String nick;
    private String text;
    private int color;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        setColor();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getColor() {
        return color;
    }

    private void setColor() {
        switch (type) {
            case "CLAN":
                color = 0xFFB7B5F9;
                break;
            case "HERO_VOICE":
                color = 0xFF00a1e0;
                break;
            case "ALL":
                color = 0xFFffffff;
                break;
            case "PRIVATE":
            case "PRIVATE CHAT":
                color = 0xFFff639a;
                break;
            case "PARTY":
                color = 0xFF8ff46d;
                break;
            case "SHOUT":
                color = 0xFFffc24f;
                break;
            case "TRADE":
                color = 0xFFffd2fb;
                break;
            case "Unknown":
                color = 0xFFffd2fb;
                break;
        }
    }
}
