package com.l2jpane.obj;

import com.l2jpane.L2Resources;
import com.l2jpane.singletone.CurrentServer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Aleksey Vishnyakov on 22.05.2016.
 */
public class SearchResultItem {
    private String login = "";
    private String ip = "";
    private String email = "N/A";
    private String hwid = "N/A";
    private String lastAccess = "";
    private String nick = "";
    private String prof = "";
    private String level = "";
    private String status = "";

    private int accessLevel = 0;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHwid() {
        return hwid;
    }

    public void setHwid(String hwid) {
        this.hwid = hwid;
    }

    public String getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(String lastAccess) {
        this.lastAccess = lastAccess.substring(0, lastAccess.length() - 2);
    }

    public void setLastAccess(long temp) {
        if (temp < 10000000000L) {
            temp *= 1000;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
        Date resultdate = new Date(temp);
        lastAccess = sdf.format(resultdate);
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        if (CurrentServer.getInstance().getCurrentBuild().containsKey("altAccesslevel") &&
                CurrentServer.getInstance().getCurrentBuild().get("altAccesslevel").equals("true")) {
            if (accessLevel == 1)
                this.accessLevel = -100;
        }
        this.accessLevel = accessLevel;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = L2Resources.getProfName(prof);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        switch (status) {
            case "1":
                this.status = "ONLINE";
                break;
            case "0":
                this.status = "OFFLINE";
                break;
            default:
                this.status = status;
                break;
        }
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
