package com.l2jpane.obj;


import android.content.Context;
import android.graphics.Bitmap;

import com.l2jpane.App;
import com.l2jpane.L2Resources;
import com.l2jpane.R;

/**
 * Created by aleksey on 08.06.16.
 */
public class InventoryItem {
    private int itemId = -1;
    private long objItemId = -1;
    private String itemName = "";
    private String count = "";
    private String location = "";
    private int enchant = 0;
    private String elemental = "";
    private Bitmap itemIcon;
    Context context = App.getContext();

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        if (location.equalsIgnoreCase("PAPERDOLL"))
            this.location = context.getString(R.string.on_char);
        else if (location.equalsIgnoreCase("INVENTORY"))
            this.location = context.getString(R.string.in_inventory);
        else if (location.equalsIgnoreCase("WAREHOUSE"))
            this.location = context.getString(R.string.in_warehouse);
        else if (location.equalsIgnoreCase("PET"))
            this.location = context.getString(R.string.on_pet);
        else if (location.equalsIgnoreCase("MAIL"))
            this.location = context.getString(R.string.in_mailbox);
        else if (location.equalsIgnoreCase("CLANWH"))
            this.location = context.getString(R.string.in_clan_wh);
        else if (location.equalsIgnoreCase("DELAYED"))
            this.location = context.getString(R.string.in_delay);
        else
            this.location = location;
    }

    public String getCount() {
        return count;
    }

    public void setCount(long _count) {
        this.count = _count > 1 ? ("x" + _count) : "";
    }

    public String getItemName() {
        if (count.isEmpty() && enchant > 0)
            return itemName + " +" + enchant;
        else
            return itemName;
    }

    public void setItemName() {
        this.itemName = L2Resources.getItemName(itemId);
    }

    public String getItemId() {
        return String.format(context.getString(R.string.id_is), itemId);
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getEnchant() {
        return enchant;
    }

    public void setEnchant(int enchant) {
        this.enchant = enchant;
    }

    public String getElemental() {
        return elemental;
    }

    public void setElemental(String elemental) {
        this.elemental = elemental;
    }

    public long getObjItemId() {
        return objItemId;
    }

    public void setObjItemId(long objItemId) {
        this.objItemId = objItemId;
    }

    public Bitmap getItemIcon() {
        return itemIcon;
    }

    public void setItemIcon() {
        this.itemIcon = L2Resources.getItemIcon(itemId);
    }
}
