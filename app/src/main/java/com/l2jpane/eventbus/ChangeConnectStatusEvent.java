package com.l2jpane.eventbus;

/**
 * Created by aleksej on 30.06.16.
 */
public class ChangeConnectStatusEvent {
    private String msg = "";

    public ChangeConnectStatusEvent (String _msg) {
        if (_msg != null)
            msg = _msg;
    }

    public String getMsg() {
        return msg;
    }
}
