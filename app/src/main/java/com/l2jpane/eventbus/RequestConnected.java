package com.l2jpane.eventbus;

import java.sql.Connection;

/**
 * Created by aleksey on 15.06.16.
 */
public class RequestConnected {
    private Connection loginSQL;
    private Connection gameSQL;

    public RequestConnected(Connection login, Connection game) {
        loginSQL = login;
        gameSQL = game;
    }
    public Connection getLoginSQL() {
        return loginSQL;
    }

    public Connection getGameSQL() {
        return gameSQL;
    }
}
