package com.l2jpane.eventbus;

/**
 * Created by aleksey on 07.06.16.
 */
public class ShowLoadingEvent {
    private String message = "";

    public ShowLoadingEvent(String _message) {
        message = _message;
    }

    public String getMessage() {
        return message;
    }
}
