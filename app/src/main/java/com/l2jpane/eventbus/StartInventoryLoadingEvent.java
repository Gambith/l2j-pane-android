package com.l2jpane.eventbus;

/**
 * Created by Aleksey Vishnyakov on 08.06.2016.
 */
public class StartInventoryLoadingEvent {
    private long charId = -1;
    private long clanId = -1;

    public StartInventoryLoadingEvent(long charId, long clanId) {
        this.charId = charId;
        this.clanId = clanId;
    }

    public long getClanId() {
        return clanId;
    }

    public long getCharId() {
        return charId;
    }
}
