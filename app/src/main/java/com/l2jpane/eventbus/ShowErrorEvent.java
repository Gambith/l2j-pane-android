package com.l2jpane.eventbus;

import android.util.Log;

/**
 * Created by Aleksey Vishnyakov on 21.06.2016.
 */
public class ShowErrorEvent {
    private String shortError = "";
    private String fullError = "";

    public ShowErrorEvent(String _shortError, String _fullError) {
        shortError = _shortError;
        fullError = _fullError;
        Log.d("error", "event created");
    }

    public String getShortError() {
        return shortError;
    }

    public String getFullError() {
        return fullError;
    }
}
