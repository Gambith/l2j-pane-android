package com.l2jpane;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Aleksey Vishnyakov on 26.05.2016.
 */
public class L2Resources {
    public static String getProfName(String classid) {
        int nameResourceID = App.getContext().getResources().getIdentifier("prof" + classid, "string",
                App.getContext().getApplicationInfo().packageName);
        if (nameResourceID != 0)
            return App.getContext().getResources().getString(nameResourceID);
        else return classid;
    }

    public static String getItemName(int itemId) {
        int nameResourceID = App.getContext().getResources().getIdentifier("item" + itemId, "string",
                App.getContext().getApplicationInfo().packageName);
        try {
            return App.getContext().getResources().getString(nameResourceID);
        } catch (Resources.NotFoundException e) {
            nameResourceID = App.getContext().getResources().getIdentifier("item0", "string",
                    App.getContext().getApplicationInfo().packageName);
            return App.getContext().getResources().getString(nameResourceID);
        }
    }

    public static Bitmap getItemIcon(int itemId) {
        InputStream is = null;
        try {
            is = App.getContext().getResources().getAssets().open("items/item_icon_" + itemId + ".png");
        } catch (IOException e) {
            try {
                is = App.getContext().getResources().getAssets().open("items/item_icon_0.png");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        return BitmapFactory.decodeStream(is);
    }

    public static String getElement(int id) {
        Context context = App.getContext();
        switch (id) {
            case 0:
                return context.getString(R.string.fire);
            case 1:
                return context.getString(R.string.water);
            case 2:
                return context.getString(R.string.wind);
            case 3:
                return context.getString(R.string.earth);
            case 4:
                return context.getString(R.string.holy);
            case 5:
                return context.getString(R.string.unholy);
            default:
                return String.valueOf(id);
        }
    }
}
