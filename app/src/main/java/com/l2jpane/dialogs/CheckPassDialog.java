package com.l2jpane.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.l2jpane.R;

public class CheckPassDialog extends android.app.DialogFragment
        implements TextView.OnEditorActionListener, View.OnClickListener {


    private EditText passField;
    private String passPhrase;
    public static Long ts = null;

    public interface CheckPassDialogListener {
        void onFinishEditDialog(String inputText);
    }


    @Override
    public void onClick(View v) {
        CheckPassDialogListener activity = (CheckPassDialogListener) getActivity();
        activity.onFinishEditDialog(passField.getText().toString());
        boolean passed = false;
        if (passField.length() > 0 && passPhrase != null
                && passPhrase.equals(passField.getText().toString()))
            passed = true;
        else if (passField.length() > 0 && passPhrase == null) {
            SharedPreferences passFile = getActivity().getSharedPreferences("passphrase", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = passFile.edit();
            passPhrase = passField.getText().toString();
            editor.putString("phrase", passPhrase);
            editor.apply();
            passed = true;        }
        if (passed)
            this.dismiss();
        else {
            ts = null;
            getActivity().finish();
        }

    }

    public static CheckPassDialog newInstance(int num) {
        CheckPassDialog f = new CheckPassDialog();

        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View form = getActivity().getLayoutInflater()
                .inflate(R.layout.checkpassdialog, null);
        passField = (EditText) form.findViewById(R.id.passField);
        final Button button = (Button) form.findViewById(R.id.okPassBtn);
        passPhrase = getActivity().getSharedPreferences("passphrase", Context.MODE_PRIVATE).getString("phrase", null);
        passField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                if (passField.getText().toString().equals(passPhrase))
                    dismiss();
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        button.setOnClickListener(this);
        String title;
        if (passPhrase == null)
            title = getString(R.string.newpassphrasetitle);
        else
            title = getString(R.string.fillpassphrasetitle);
        return (builder.setTitle(title).setView(form).create());
    }
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            CheckPassDialogListener activity = (CheckPassDialogListener) getActivity();
            activity.onFinishEditDialog(passField.getText().toString());
            this.dismiss();
            return true;
        }
        return false;
    }

}