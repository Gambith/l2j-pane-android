package com.l2jpane.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.l2jpane.L2Resources;
import com.l2jpane.R;
import com.l2jpane.database.SQL;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.ShowLoadingEvent;
import com.l2jpane.eventbus.StartInventoryLoadingEvent;
import com.l2jpane.singletone.CurrentServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AddItemDialog extends DialogFragment implements
        DialogInterface.OnClickListener {

    private EditText itemIdET;
    private EditText itemEnchantCountET;
    private Spinner spinner1;
    private Spinner spinner2;
    private Spinner spinner3;
    private EditText elementET1;
    private EditText elementET2;
    private EditText elementET3;
    private ImageView itemIconIV;
    private TextView itemNameTV;
    private int itemId = -1;
    private boolean delayed;
    private boolean stackable;
    private boolean enableElements;
    private boolean delayWithoutEnchant;
    private String nick;
    private long charId;
    private long clanId;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        nick = getActivity().getIntent().getStringExtra("nick");
        charId = getArguments().getLong("charId");
        clanId = getArguments().getLong("clanId");
        delayed = getArguments().getBoolean("delayed");
        delayWithoutEnchant = getArguments().getBoolean("delayWithoutEnchant");
        boolean delayWithoutEnchant = getArguments().getBoolean("delayWithoutEnchant");
        stackable = getArguments().getBoolean("stackable");
        enableElements = getArguments().getBoolean("enableElements");
        final View form;
        if (stackable || delayed || !enableElements) {
            form = getActivity().getLayoutInflater()
                    .inflate(R.layout.add_stack_item_dialog, null);
        } else {
            form = getActivity().getLayoutInflater()
                    .inflate(R.layout.add_nonstack_item_dialog, null);
            spinner1 = (Spinner) form.findViewById(R.id.spinner1);
            spinner2 = (Spinner) form.findViewById(R.id.spinner2);
            spinner3 = (Spinner) form.findViewById(R.id.spinner3);
            elementET1 = (EditText) form.findViewById(R.id.elementET1);
            elementET2 = (EditText) form.findViewById(R.id.elementET2);
            elementET3 = (EditText) form.findViewById(R.id.elementET3);
            AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int visiblity;
                    if (position == 0)
                        visiblity = View.INVISIBLE;
                    else
                        visiblity = View.VISIBLE;
                    if (parent.getId() == spinner1.getId())
                        elementET1.setVisibility(visiblity);
                    else if (parent.getId() == spinner2.getId())
                        elementET2.setVisibility(visiblity);
                    else if (parent.getId() == spinner3.getId())
                        elementET3.setVisibility(visiblity);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            };
            spinner1.setOnItemSelectedListener(listener);
            spinner2.setOnItemSelectedListener(listener);
            spinner3.setOnItemSelectedListener(listener);
        }

        itemIconIV = (ImageView) form.findViewById(R.id.itemIconIV);
        itemEnchantCountET = (EditText) form.findViewById(R.id.itemEnchantCountET);
        if ((delayed || enableElements) && !stackable) {
            itemEnchantCountET.setHint(getString(R.string.enchant));
            if (delayWithoutEnchant)
                itemEnchantCountET.setEnabled(false);
        }
        itemIdET = (EditText) form.findViewById(R.id.itemIdET);
        itemNameTV = (TextView) form.findViewById(R.id.itemNameTV);
        if (savedInstanceState != null) {
            itemId = savedInstanceState.getInt("itemId");
            setTitle(itemId);
            Log.d("itemId", itemId + "");
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return (builder.setView(form)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, null).create());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        itemIdET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                setTitle(-1);
            }
        });
    }

    private void setTitle(int id) {
        try {
            if (id == -1)
                this.itemId = Integer.parseInt(itemIdET.getText().toString());
            itemNameTV.setText(L2Resources.getItemName(this.itemId));
            itemIconIV.setImageBitmap(L2Resources.getItemIcon(this.itemId));
        } catch (NumberFormatException e) {
            itemIconIV.setImageBitmap(null);
            itemNameTV.setText(null);
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (itemIdET.getText().toString().isEmpty() || itemEnchantCountET.getText().toString().isEmpty() ||
                (!delayed && enableElements && !stackable &&
                        ((spinner1.getSelectedItemPosition() != 0 && elementET1.getText().toString().isEmpty()) ||
                                (spinner2.getSelectedItemPosition() != 0 && elementET2.getText().toString().isEmpty()) ||
                                (spinner3.getSelectedItemPosition() != 0 && elementET3.getText().toString().isEmpty())))) {

            Toast.makeText(getActivity(), getString(R.string.hasnullfields), Toast.LENGTH_SHORT).show();
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            private int itemEnchantCount;
            private int[][] elements = new int[3][2];

            @Override
            protected void onPreExecute() {
                if (!stackable) {
                    if (spinner1.getSelectedItemPosition() != 0) {
                        elements[0][0] = Integer.parseInt(elementET1.getText().toString());
                        elements[0][1] = spinner1.getSelectedItemPosition() - 1;
                    }
                    if (spinner2.getSelectedItemPosition() != 0) {
                        elements[1][0] = Integer.parseInt(elementET2.getText().toString());
                        elements[1][1] = spinner2.getSelectedItemPosition() - 1;

                    }
                    if (spinner3.getSelectedItemPosition() != 0) {
                        elements[2][0] = Integer.parseInt(elementET3.getText().toString());
                        elements[2][1] = spinner3.getSelectedItemPosition() - 1;

                    }
                }
                itemEnchantCount = Integer.parseInt(itemEnchantCountET.getText().toString());
                BusProvider.getInstance().post(new ShowLoadingEvent(String.format(getString(R.string.inventory_loading), nick)));
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Statement st = CurrentServer.getInstance().getGameSQL().createStatement();
                    long nextObjId = 0;
                    if (!delayed) {
                        ResultSet rs = st.executeQuery(SQL.getLastItemObjId());
                        if (rs.next())
                            nextObjId = rs.getLong(SQL.itemsObjIdColumn) + 1;
                        rs.close();
                    }
                    if (stackable)
                        st.executeUpdate(SQL.createStackItem(delayed, charId, nextObjId, itemId, itemEnchantCount));
                    else {
                        st.executeUpdate(SQL.createNonStackItem(
                                delayed, delayWithoutEnchant, charId, nextObjId, itemId, itemEnchantCount));
                        if (enableElements) {
                            for (int i = 0; 3 > i; i++) {
                                if (elements[i][0] != 0)
                                    st.executeUpdate(SQL.addElements(nextObjId, elements[i][1], elements[i][0]));
                            }
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                BusProvider.getInstance().post(new StartInventoryLoadingEvent(charId, clanId));
            }
        }.execute();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("itemId", itemId);
        super.onSaveInstanceState(outState);
    }
}
