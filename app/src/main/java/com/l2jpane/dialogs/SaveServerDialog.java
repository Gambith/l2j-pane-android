package com.l2jpane.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.l2jpane.R;
import com.l2jpane.singletone.CurrentServer;

import java.io.File;
import java.util.Map;

public class SaveServerDialog extends DialogFragment implements
        DialogInterface.OnClickListener {
    private EditText nameField;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View form = getActivity().getLayoutInflater()
                .inflate(R.layout.createserverdialog, null);
        nameField = (EditText) form.findViewById(R.id.nameField);
        if (CurrentServer.getInstance().getSelectedServerName() != null) {
            nameField.setText(CurrentServer.getInstance().getSelectedServerName());
            nameField.setSelectAllOnFocus(true);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return (builder.setTitle(R.string.fillServerName).setView(form)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, null).create());
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (nameField.length() < 3 || nameField.length() > 15)
            Toast.makeText(getActivity(), R.string.errservername, Toast.LENGTH_LONG).show();
        else {
            if (CurrentServer.getInstance().getSelectedServerName() == null)
                Toast.makeText(getActivity(), R.string.cfgcreated, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(getActivity(), R.string.cfgchanged, Toast.LENGTH_LONG).show();

            savePreferences();
            NavUtils.navigateUpFromSameTask(getActivity());
            getActivity().finish();
        }
    }

    private void savePreferences() {
        if (CurrentServer.getInstance().getSelectedServerName() != null)
            new File("data/data/com.l2jpane/shared_prefs/srv_" + CurrentServer.getInstance().getSelectedServerName() + ".xml").delete();

        SharedPreferences tempPref =
                getActivity().getSharedPreferences("com.l2jpane_preferences", Context.MODE_PRIVATE);
        SharedPreferences newServerSP = getActivity().
                getSharedPreferences("srv_" + nameField.getText().toString(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = newServerSP.edit();

        Map<String, ?> allEntries = tempPref.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue().toString());
        }
        editor.apply();
        tempPref.edit().clear().apply();

    }
}
