package com.l2jpane.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.l2jpane.R;
import com.l2jpane.database.ConnectTask;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.ChangeConnectStatusEvent;
import com.l2jpane.eventbus.DismissConnectionDialogEvent;
import com.squareup.otto.Subscribe;

public class LoadingDialog extends DialogFragment {

    private TextView statusTV;

    @Subscribe
    public void setStatusMsg(ChangeConnectStatusEvent event) {
        statusTV.setText(event.getMsg());
    }

    @Subscribe
    public void onDissmissEvent (DismissConnectionDialogEvent event) {
        dismiss();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View form = getActivity().getLayoutInflater()
                .inflate(R.layout.loadingdialog, null);
        statusTV = (TextView) form.findViewById(R.id.statusTV);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ((ProgressBar) form.findViewById(R.id.progressBar))
                .getIndeterminateDrawable()
                .setColorFilter(0xFF8aff8c, PorterDuff.Mode.SRC_IN);
        if (savedInstanceState != null)
            statusTV.setText(savedInstanceState.getString("status"));
        return (builder.setView(form).create());
    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getActivity().setContentView(R.layout.loadingdialog);
    }

    @Override
    public void onResume() {
        ConnectTask connectTask = ConnectTask.getInstance();
        if (connectTask.getStatus() != AsyncTask.Status.RUNNING) {
            try {
                connectTask.execute();
            } catch (IllegalStateException e) {
                ConnectTask.newInstance().execute();
            }
        }
        BusProvider.getInstance().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("status", statusTV.getText().toString());
    }
}



