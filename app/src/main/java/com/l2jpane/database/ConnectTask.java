package com.l2jpane.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.jcraft.jsch.JSchException;
import com.l2jpane.App;
import com.l2jpane.Builds;
import com.l2jpane.R;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.ChangeConnectStatusEvent;
import com.l2jpane.eventbus.DismissConnectionDialogEvent;
import com.l2jpane.eventbus.RequestConnected;
import com.l2jpane.singletone.CurrentServer;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by aleksej on 30.06.16.
 */
public class ConnectTask extends AsyncTask<Void, String, Void> {

    private static ConnectTask connectTask = new ConnectTask();
    public static ConnectTask getInstance() {
        if (connectTask == null && connectTask.getStatus() != Status.RUNNING && connectTask.getStatus() != Status.PENDING && connectTask.getStatus() != Status.FINISHED)
            newInstance();
        return connectTask;
    }

    public static ConnectTask newInstance () {
        connectTask = new ConnectTask();
        return connectTask;
    }

    private java.sql.Connection[] connections;
    private String msg;
    private String getConnection = App.getContext().getString(R.string.getconnection);
    private String loginserver = App.getContext().getString(R.string.loginserver);
    private String gameserver = App.getContext().getString(R.string.gameserver);
    private String errorHost = App.getContext().getString(R.string.erroraddressreachable);
    private String errorDB = App.getContext().getString(R.string.errorconnectiondb);
    private String errorSSH = App.getContext().getString(R.string.errorconnectionssh);
    private String goodConnection = App.getContext().getString(R.string.connectionenstablished);
    private String checkresolve = App.getContext().getString(R.string.checkresolve);
    private DBConnection connection = new DBConnection();
    private Map<String, String> cfg;

    @Override
    protected void onPreExecute() {
        cfg = connection.readCFG(App.getContext()
                .getSharedPreferences("srv_" + CurrentServer.getInstance().getSelectedServerName(),
                        Context.MODE_PRIVATE));
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            java.sql.Connection loginSQL;
            java.sql.Connection gameSQL;
            BusProvider.getInstance().post(new ChangeConnectStatusEvent(loginserver + checkresolve));
            if (!connection.resolveHost(cfg, "ls")) {
                msg = loginserver + errorHost;
                cancel(false);
                return null;
            } else {
                publishProgress(gameserver + checkresolve);
            }
            if (!connection.resolveHost(cfg, "gs")) {
                msg = gameserver + errorHost;
                cancel(false);
                return null;
            }
            publishProgress(loginserver + getConnection);
            msg = loginserver;
            loginSQL = connection.getConnect(cfg, "ls");
            publishProgress(gameserver + getConnection);
            msg = gameserver;
            gameSQL = connection.getConnect(cfg, "gs");
            publishProgress(goodConnection);
            connections = new java.sql.Connection[]{loginSQL, gameSQL};

        } catch (SQLException e) {
            e.printStackTrace();
            msg = msg + errorDB;
            cancel(false);
            return null;
        } catch (JSchException e) {
            e.printStackTrace();
            msg = msg + errorSSH;
            cancel(false);
            return null;
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        BusProvider.getInstance().post(new ChangeConnectStatusEvent(values[0]));
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        SharedPreferences sp =
                App.getContext()
                        .getSharedPreferences("srv_" + CurrentServer.getInstance().getSelectedServerName(),
                                Context.MODE_PRIVATE);
        CurrentServer.getInstance().setSelectedServerCFG(sp.getAll());
        CurrentServer.getInstance().setCurrentBuild(Builds.getBuild(sp.getString("build", "L2jServer")));
        if (!isCancelled() && App.getContext() != null) {
            BusProvider.getInstance().post(new RequestConnected(connections[0], connections[1]));
            BusProvider.getInstance().post(new DismissConnectionDialogEvent());
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (App.getContext() != null) {
            BusProvider.getInstance().post(new DismissConnectionDialogEvent());
            Toast.makeText(App.getContext(), msg, Toast.LENGTH_LONG).show();
        }
    }
}

