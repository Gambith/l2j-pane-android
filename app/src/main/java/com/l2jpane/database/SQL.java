package com.l2jpane.database;


import com.l2jpane.singletone.CurrentServer;

import java.util.Map;

/**
 * Created by Aleksey Vishnyakov on 23.05.2016.
 */
public class SQL {

    private static Map<String, String> currentBuild = CurrentServer.getInstance().getCurrentBuild();
    public static final String charTable = "charTable";
    public static final String charOnlineColumn = "charOnlineColumn";
    public static final String charLoginColumn = "charLoginColumn";
    public static final String charIdColumn = "charIdColumn";
    public static final String charNickColumn = "charNickColumn";
    public static final String charLastActiveColumn = "charLastActiveColumn";
    public static final String charAccessLevelColumn = "charAccessLevelColumn";
    public static final String charClanColumn = "charClanColumn";
    public static final String charSexColumn = "charSexColumn";
    public static final String charPvpColumn = "charPvpColumn";
    public static final String charPkColumn = "charPkColumn";
    public static final String charKarmaColumn = "charKarmaColumn";
    public static final String charCreateTimeColumn = "charCreateTimeColumn";
    public static final String charOnlineTimeColumn = "charOnlineTimeColumn";

    public static final String altCharTable = "altCharTable";
    public static final String altCharIdColumn = "altCharIdColumn";
    public static final String charCpColumn = "charCpColumn";
    public static final String charMaxCpColumn = "charMaxCpColumn";
    public static final String charHpColumn = "charHpColumn";
    public static final String charMaxHpColumn = "charMaxHpColumn";
    public static final String charMpColumn = "charMpColumn";
    public static final String charMaxMpColumn = "charMaxMpColumn";
    public static final String charClassIdColumn = "charClassIdColumn";
    public static final String charLevelColumn = "charLevelColumn";
    public static final String charIsBaseColumn = "charIsBaseColumn";
    public static final String charBaseClassIdColumn = "charBaseClassIdColumn";

    public static final String accTable = "accTable";
    public static final String accLoginColumn = "accLoginColumn";
    public static final String accLastActiveColumn = "accLastActiveColumn";
    public static final String accLastIpColumn = "accLastIpColumn";
    public static final String accAccessLevelColumn = "accAccessLevelColumn";

    public static final String clanTable = "clanTable";
    public static final String clanIdColumn = "clanIdColumn";
    public static final String clanNameColumn = "clanNameColumn";

    public static final String delayTable = "delayTable";
    public static final String delayOwnerColumn = "delayOwnerColumn";
    public static final String delayObjIdColumn = "delayObjIdColumn";
    public static final String delayIdColumn = "delayIdColumn";
    public static final String delayCountColumn = "delayCountColumn";
    public static final String delayEnchantColumn = "delayEnchantColumn";

    public static final String itemsTable = "itemsTable";
    public static final String itemsOwnerColumn = "itemsOwnerColumn";
    public static final String itemsEnchantColumn = "itemsEnchantColumn";
    public static final String itemsObjIdColumn = "itemsObjIdColumn";
    public static final String itemsCountColumn = "itemsCountColumn";
    public static final String itemsIdColumn = "itemsIdColumn";
    public static final String itemsLocColumn = "itemsLocColumn";
    public static final String itemsLocDataColumn = "itemsLocDataColumn";
    public static final String itemsElemFireColumn = "itemsElemFireColumn";
    public static final String itemsElemWaterColumn = "itemsElemWaterColumn";
    public static final String itemsElemWindColumn = "itemsElemWindColumn";
    public static final String itemsElemEarthColumn = "itemsElemEarthColumn";
    public static final String itemsElemHolyColumn = "itemsElemHolyColumn";
    public static final String itemsElemUnholyColumn = "delayElemValueColumn";
    public static final String elementalTable = "elementalTable";
    public static final String elementalItemIdColumn = "elementalItemIdColumn";
    public static final String elementalTypeColumn = "elementalTypeColumn";
    public static final String elementalValueColumn = "elementalValueColumn";

    public static final String lifeStoneTable = "lifeStoneTable";
    public static final String lsItemId = "lsItemId";

    public static final String accEmailColumn = "accEmailColumn";

    public static final String hwidTable = "auth_log";
    public static final String hwidLoginColumn = "account";
    public static final String hwidHwidColumn = "hwid";
    public static final String hwidDateColumn = "date";


    public static String getOnline() {
        return "SELECT " + currentBuild.get(charOnlineColumn) + " " +
                "FROM " + currentBuild.get(charTable) + " " +
                "WHERE " + currentBuild.get(charOnlineColumn) + " = 1";
    }

    public static String getAccountByLogin(String string, boolean enableEmail) {
        String str = "SELECT ";
        if (enableEmail) {
            str = str + currentBuild.get(accEmailColumn) + " " + accEmailColumn + ", ";
        }

        return str +
                currentBuild.get(accLoginColumn) + " " + accLoginColumn + ", " +
                currentBuild.get(accLastIpColumn) + " " + accLastIpColumn + ", " +
                currentBuild.get(accAccessLevelColumn) + " " + accAccessLevelColumn + ", " +
                currentBuild.get(accLastActiveColumn) + " " + accLastActiveColumn + " " +
                "FROM " + currentBuild.get(accTable) + " " +
                "WHERE " + currentBuild.get(accLoginColumn) + " LIKE '%" + string + "%' " +
                "ORDER BY " + currentBuild.get(accLastActiveColumn) + " DESC";
    }

    public static String getOneAccountByLogin(String string, boolean enableEmail) {
        String str = "SELECT ";
        if (enableEmail) {
            str = str + currentBuild.get(accEmailColumn) + " " + accEmailColumn + ", ";
        }

        return str +
                currentBuild.get(accLoginColumn) + " " + accLoginColumn + ", " +
                currentBuild.get(accLastIpColumn) + " " + accLastIpColumn + ", " +
                currentBuild.get(accAccessLevelColumn) + " " + accAccessLevelColumn + ", " +
                currentBuild.get(accLastActiveColumn) + " " + accLastActiveColumn + " " +
                "FROM " + currentBuild.get(accTable) + " " +
                "WHERE " + currentBuild.get(accLoginColumn) + " = '" + string + "' " +
                "ORDER BY " + currentBuild.get(accLastActiveColumn) + " DESC";
    }

    public static String getAccountByIP(String string, boolean enableEmail) {
        String str = "SELECT ";
        if (enableEmail) {
            str = str + currentBuild.get(accEmailColumn) + " " + accEmailColumn + ", ";
        }

        return str +
                currentBuild.get(accLoginColumn) + " " + accLoginColumn + ", " +
                currentBuild.get(accLastIpColumn) + " " + accLastIpColumn + ", " +
                currentBuild.get(accAccessLevelColumn) + " " + accAccessLevelColumn + ", " +
                currentBuild.get(accLastActiveColumn) + " " + accLastActiveColumn + " " +
                "FROM " + currentBuild.get(accTable) + " " +
                "WHERE " + currentBuild.get(accLastIpColumn) + " LIKE '%" + string + "%' " +
                "ORDER BY " + currentBuild.get(accLastActiveColumn) + " DESC";
    }

    public static String getAccountByEmail(String string) {
        return "SELECT " +
                currentBuild.get(accEmailColumn) + " " + accEmailColumn + ", " +
                currentBuild.get(accLoginColumn) + " " + accLoginColumn + ", " +
                currentBuild.get(accLastIpColumn) + " " + accLastIpColumn + ", " +
                currentBuild.get(accAccessLevelColumn) + " " + accAccessLevelColumn + ", " +
                currentBuild.get(accLastActiveColumn) + " " + accLastActiveColumn + " " +
                "FROM " + currentBuild.get(accTable) + " " +
                "WHERE " + currentBuild.get(accEmailColumn) + " LIKE '%" + string + "%' " +
                "ORDER BY " + currentBuild.get(accLastActiveColumn) + " DESC";
    }

    public static String getLoginByNick(String string) {
        return "SELECT " + currentBuild.get(charLoginColumn) + " " + charLoginColumn + " " +
                "FROM " + currentBuild.get(charTable) + " " +
                "WHERE " + currentBuild.get(charNickColumn) + " = '" + string + "'";
    }

    public static String getLoginsByHWID(String string) {
        return "SELECT " + hwidLoginColumn + " " + hwidLoginColumn + " " +
                "FROM " + hwidTable + " " +
                "WHERE " + hwidHwidColumn + " LIKE '%" + string + "%'";
    }

    public static String getLastHWID(String string) {
        return "SELECT " + hwidHwidColumn + " " + hwidHwidColumn + " " +
                "FROM " + hwidTable + " " +
                "WHERE " + hwidLoginColumn + " = '" + string + "'";
    }

    public static String getAccounts() {
        return "SELECT " + currentBuild.get(accLoginColumn) + " " +
                "FROM " + currentBuild.get(accTable);
    }

    public static String getCharacters() {
        return "SELECT " + currentBuild.get(charNickColumn) + " " +
                "FROM " + currentBuild.get(charTable);
    }

    public static String getCharByLogin(String string, boolean altCharInfo) {
        String result = "SELECT " +
                currentBuild.get(charLoginColumn) + " " + charLoginColumn + ", " +
                currentBuild.get(charNickColumn) + " " + charNickColumn + ", " +
                currentBuild.get(charOnlineColumn) + " " + charOnlineColumn + ", " +
                currentBuild.get(charClassIdColumn) + " " + charClassIdColumn + ", " +
                currentBuild.get(charLevelColumn) + " " + charLevelColumn + ", " +
                currentBuild.get(charAccessLevelColumn) + " " + charAccessLevelColumn + ", " +
                currentBuild.get(charLastActiveColumn) + " " + charLastActiveColumn + " ";
        if (altCharInfo)
            return result +
                    "FROM " + currentBuild.get(charTable) + ", " + currentBuild.get(altCharTable) + " " +
                    "WHERE " +
                    currentBuild.get(charLoginColumn) + " LIKE '%" + string + "%' " + "AND " +
                    "active = '1' AND " +
                    currentBuild.get(charTable) + "." + currentBuild.get(charIdColumn) + " = " +
                    currentBuild.get(altCharTable) + "." + currentBuild.get(altCharIdColumn) + " " +
                    "ORDER BY " + currentBuild.get(charLastActiveColumn) + " DESC";
        else
            return result +
                    "FROM " + currentBuild.get(charTable) + " " +
                    "WHERE " + currentBuild.get(charLoginColumn) + " LIKE '%" + string + "%' " +
                    "ORDER BY " + currentBuild.get(charLastActiveColumn) + " DESC";
    }

    public static String getCharByNick(String string, boolean altCharInfo) {
        String result = "SELECT " +
                currentBuild.get(charLoginColumn) + " " + charLoginColumn + ", " +
                currentBuild.get(charNickColumn) + " " + charNickColumn + ", " +
                currentBuild.get(charOnlineColumn) + " " + charOnlineColumn + ", " +
                currentBuild.get(charClassIdColumn) + " " + charClassIdColumn + ", " +
                currentBuild.get(charLevelColumn) + " " + charLevelColumn + ", " +
                currentBuild.get(charAccessLevelColumn) + " " + charAccessLevelColumn + ", " +
                currentBuild.get(charLastActiveColumn) + " " + charLastActiveColumn + " ";
        if (altCharInfo)
            return result +
                    "FROM " + currentBuild.get(charTable) + ", " + currentBuild.get(altCharTable) + " " +
                    "WHERE " +
                    currentBuild.get(charNickColumn) + " LIKE '%" + string + "%' " + "AND " +
                    "active = '1' AND " +
                    currentBuild.get(charTable) + "." + currentBuild.get(charIdColumn) + " = " +
                    currentBuild.get(altCharTable) + "." + currentBuild.get(altCharIdColumn) + " " +
                    "ORDER BY " + currentBuild.get(charLastActiveColumn) + " DESC";
        else
            return result +
                    "FROM " + currentBuild.get(charTable) + " " +
                    "WHERE " + currentBuild.get(charNickColumn) + " LIKE '%" + string + "%' " +
                    "ORDER BY " + currentBuild.get(charLastActiveColumn) + " DESC";
    }

    public static String getHwidByLogin(String string) {
        return "SELECT * " +
                "FROM " + hwidTable + " " +
                "WHERE " + hwidLoginColumn + " = '" + string + "' " +
                "ORDER BY " + hwidDateColumn + " DESC";
    }

    public static String getHwidByNick(String string) {
        return "SELECT * " +
                "FROM " + hwidTable + " " +
                "WHERE " + hwidLoginColumn + " = " +
                "(" +
                "SELECT " + currentBuild.get(charLoginColumn) + " " +
                "FROM " + currentBuild.get(charTable) + " " +
                "WHERE " + currentBuild.get(charNickColumn) + " = '" + string + "'" +
                ") " +
                "ORDER BY " + hwidDateColumn + " DESC";
    }

    public static String getCharactersOnAcc(String string, boolean altCharInfo) {
        String result = "SELECT " +
                currentBuild.get(charLoginColumn) + " " + charLoginColumn + ", " +
                currentBuild.get(charNickColumn) + " " + charNickColumn + ", " +
                currentBuild.get(charOnlineColumn) + " " + charOnlineColumn + ", " +
                currentBuild.get(charClassIdColumn) + " " + charClassIdColumn + ", " +
                currentBuild.get(charLevelColumn) + " " + charLevelColumn + ", " +
                currentBuild.get(charAccessLevelColumn) + " " + charAccessLevelColumn + ", " +
                currentBuild.get(charLastActiveColumn) + " " + charLastActiveColumn + " ";
        if (altCharInfo)
            return result +
                    "FROM " + currentBuild.get(charTable) + ", " + currentBuild.get(altCharTable) + " " +
                    "WHERE " +
                    currentBuild.get(charLoginColumn) + " = '" + string + "' " + "AND " +
                    "active = '1' AND " +
                    currentBuild.get(charTable) + "." + currentBuild.get(charIdColumn) + " = " +
                    currentBuild.get(altCharTable) + "." + currentBuild.get(altCharIdColumn) + " " +
                    "ORDER BY " + currentBuild.get(charLastActiveColumn) + " DESC";
        else
            return result +
                    "FROM " + currentBuild.get(charTable) + " " +
                    "WHERE " + currentBuild.get(charLoginColumn) + " = '" + string + "' " +
                    "ORDER BY " + currentBuild.get(charLastActiveColumn) + " DESC";
    }

    public static String banAcc(String login) {
        return "UPDATE " + currentBuild.get(accTable) + " " +
                "SET " + currentBuild.get(accAccessLevelColumn) + " = -100 " +
                "WHERE " + currentBuild.get(accLoginColumn) + " = '" + login + "'";
    }

    public static String unbanAcc(String login) {
        return "UPDATE " + currentBuild.get(accTable) + " " +
                "SET " + currentBuild.get(accAccessLevelColumn) + " = 0 " +
                "WHERE " + currentBuild.get(accLoginColumn) + " = '" + login + "'";
    }

    public static String banChar(String nick, boolean altCharInfo) {
        int value;
        if (altCharInfo)
            value = 1;
        else
            value = -100;

        return "UPDATE " + currentBuild.get(charTable) + " " +
                "SET " + currentBuild.get(charAccessLevelColumn) + " = " + value + " " +
                    "WHERE " + currentBuild.get(charNickColumn) + " = '" + nick + "'";

    }

    public static String unbanChar(String nick) {
        return "UPDATE " + currentBuild.get(charTable) + " " +
                "SET " + currentBuild.get(charAccessLevelColumn) + " = 0 " +
                "WHERE " + currentBuild.get(charNickColumn) + " = '" + nick + "'";
    }

    public static String changeLogin(String newLogin, String nick) {
        return "UPDATE " + currentBuild.get(charTable) + " " +
                "SET " + currentBuild.get(charLoginColumn) + " = '" + newLogin + "' " +
                "WHERE " + currentBuild.get(charNickColumn) + " = '" + nick + "'";
    }

    public static String changeEmail(String newEmail, String login) {
        return "UPDATE " + currentBuild.get(accTable) + " " +
                "SET " + currentBuild.get(accEmailColumn) + " = '" + newEmail + "' " +
                "WHERE " + currentBuild.get(accLoginColumn) + " = '" + login + "'";
    }

    public static String getCharacterInfo(String nick, boolean altCharInfo, boolean enableCreateTime) {
        String result = "SELECT " +
                currentBuild.get(clanIdColumn) + " " + clanIdColumn + ", " +
                currentBuild.get(clanNameColumn) + " " + clanNameColumn + ", " +
                currentBuild.get(charLoginColumn) + " " + charLoginColumn + ", " +
                currentBuild.get(charIdColumn) + " " + charIdColumn + ", " +
                currentBuild.get(charNickColumn) + " " + charNickColumn + ", " +
                currentBuild.get(charOnlineColumn) + " " + charOnlineColumn + ", " +
                currentBuild.get(charClassIdColumn) + " " + charClassIdColumn + ", " +
                currentBuild.get(charLevelColumn) + " " + charLevelColumn + ", " +
                currentBuild.get(charAccessLevelColumn) + " " + charAccessLevelColumn + ", " +
                currentBuild.get(charSexColumn) + " " + charSexColumn + ", " +
                currentBuild.get(charPvpColumn) + " " + charPvpColumn + ", " +
                currentBuild.get(charPkColumn) + " " + charPkColumn + ", " +
                currentBuild.get(charKarmaColumn) + " " + charKarmaColumn + ", " +
                currentBuild.get(charCpColumn) + " " + charCpColumn + ", " +
                currentBuild.get(charMaxCpColumn) + " " + charMaxCpColumn + ", " +
                currentBuild.get(charHpColumn) + " " + charHpColumn + ", " +
                currentBuild.get(charMaxHpColumn) + " " + charMaxHpColumn + ", " +
                currentBuild.get(charMpColumn) + " " + charMpColumn + ", " +
                currentBuild.get(charMaxMpColumn) + " " + charMaxMpColumn + ", " +
                currentBuild.get(charOnlineTimeColumn) + " " + charOnlineTimeColumn + ", ";
        if (enableCreateTime)
            result = result +
                    currentBuild.get(charCreateTimeColumn) + " " + charCreateTimeColumn + ", ";
        result = result +
                currentBuild.get(charLastActiveColumn) + " " + charLastActiveColumn + " ";
        if (altCharInfo)
            return result +
                    "FROM " + currentBuild.get(charTable) + ", " + currentBuild.get(altCharTable) + " " +
                    "LEFT JOIN " + currentBuild.get(clanTable) + " ON " +
                    currentBuild.get(charTable) + "." + currentBuild.get(charClanColumn) + " = " +
                    currentBuild.get(clanTable) + "." + currentBuild.get(clanIdColumn) + " " +
                    "WHERE " +
                    "active = '1' AND " +
                    currentBuild.get(charTable) + "." + currentBuild.get(charIdColumn) + " = " +
                    currentBuild.get(altCharTable) + "." + currentBuild.get(altCharIdColumn) + " AND " +
                    currentBuild.get(charNickColumn) + " = '" + nick + "'";
        else
            return result +
                    "FROM " + currentBuild.get(charTable) + " " +
                    "LEFT JOIN " + currentBuild.get(clanTable) + " ON " +
                    currentBuild.get(charTable) + "." + currentBuild.get(charClanColumn) + " = " +
                    currentBuild.get(clanTable) + "." + currentBuild.get(clanIdColumn) + " " +
                    "WHERE " +
                    currentBuild.get(charNickColumn) + " = '" + nick + "'";
    }

    public static String getCharacterBaseProf(long charId, boolean altCharInfo) {
        if (altCharInfo)
            return "SELECT " +
                    currentBuild.get(charClassIdColumn) + " " + charIsBaseColumn + " " +
                    "FROM " + currentBuild.get(altCharTable) + " " +
                    "WHERE " + currentBuild.get(altCharIdColumn) + " = " + charId + " " +
                    "AND " + currentBuild.get(charIsBaseColumn) + " = 1";
        else
            return "SELECT " +
                    currentBuild.get(charBaseClassIdColumn) + " " + charIsBaseColumn + " " +
                    "FROM " + currentBuild.get(charTable) + " " +
                    "WHERE " + currentBuild.get(charIdColumn) + " = " + charId;
    }

    public static String getCharItems(long charId, long clanId, boolean altElements) {
        String result = "SELECT ";
        if (altElements) {
            result = result +
                    currentBuild.get(itemsElemFireColumn) + " " + itemsElemFireColumn + ", " +
                    currentBuild.get(itemsElemWaterColumn) + " " + itemsElemWaterColumn + ", " +
                    currentBuild.get(itemsElemWindColumn) + " " + itemsElemWindColumn + ", " +
                    currentBuild.get(itemsElemEarthColumn) + " " + itemsElemEarthColumn + ", " +
                    currentBuild.get(itemsElemHolyColumn) + " " + itemsElemHolyColumn + ", " +
                    currentBuild.get(itemsElemUnholyColumn) + " " + itemsElemUnholyColumn + ", ";
        }
        return result +
                currentBuild.get(itemsObjIdColumn) + " " + itemsObjIdColumn + ", " +
                currentBuild.get(itemsIdColumn) + " " + itemsIdColumn + ", " +
                currentBuild.get(itemsCountColumn) + " " + itemsCountColumn + ", " +
                currentBuild.get(itemsLocColumn) + " " + itemsLocColumn + ", " +
                currentBuild.get(itemsEnchantColumn) + " " + itemsEnchantColumn + " " +
                "FROM " + currentBuild.get(itemsTable) + " " +
                "WHERE " + currentBuild.get(itemsOwnerColumn) + " = " + charId + " " +
                "OR " + currentBuild.get(itemsOwnerColumn) + " = " + clanId;
    }

    public static String getCharDelayedItems(long charId) {
        return "SELECT " +
                currentBuild.get(delayObjIdColumn) + " " + delayObjIdColumn + ", " +
                currentBuild.get(delayIdColumn) + " " + delayIdColumn + ", " +
                currentBuild.get(delayCountColumn) + " " + delayCountColumn + ", " +
                currentBuild.get(delayEnchantColumn) + " " + delayEnchantColumn + " " +
                "FROM " + currentBuild.get(delayTable) + " " +
                "WHERE " + currentBuild.get(delayOwnerColumn) + " = " + charId;
    }

    public static String getItemElement(long itemId) {
        return "SELECT " +
                currentBuild.get(elementalTypeColumn) + " " + elementalTypeColumn + ", " +
                currentBuild.get(elementalValueColumn) + " " + elementalValueColumn + " " +
                "FROM " + currentBuild.get(elementalTable) + " " +
                "WHERE " + currentBuild.get(elementalItemIdColumn) + " = " + itemId;
    }

    public static String getLastItemObjId() {
        return "SELECT " + currentBuild.get(itemsObjIdColumn) + " " + itemsObjIdColumn + " " +
                "FROM " + currentBuild.get(itemsTable) + " " +
                "ORDER BY " + currentBuild.get(itemsObjIdColumn) + " DESC LIMIT 0,1";
    }

    public static String createStackItem(boolean delayed, long charId, long nextObjId, int itemId, int itemCount) {
        if (delayed)
            return "INSERT INTO " + currentBuild.get(delayTable) + " " +
                    "(" + currentBuild.get(delayOwnerColumn) + ", " + currentBuild.get(delayIdColumn) + ", " +
                    currentBuild.get(delayCountColumn) + ", " + currentBuild.get(delayEnchantColumn) + ") " +
                    "VALUES (" + charId + ", " + itemId + ", " + itemCount + ", 0)";
        else
            return "INSERT INTO " + currentBuild.get(itemsTable) + " " +
                    "(" + currentBuild.get(itemsOwnerColumn) + ", " + currentBuild.get(itemsObjIdColumn) + ", " +
                    currentBuild.get(itemsIdColumn) + ", " + currentBuild.get(itemsCountColumn) + ", " +
                    currentBuild.get(itemsLocColumn) + ", " + currentBuild.get(itemsLocDataColumn) + ") " +
                    "VALUES (" + charId + ", " + nextObjId + ", " + itemId + ", " + itemCount + ", 'INVENTORY', -1)";
    }

    public static String createNonStackItem(boolean delayed, boolean delayWithoutEnchant,
                                            long charId, long nextObjId, int itemId, int itemEnchant) {
        if (delayed)
            if (delayWithoutEnchant)
                return "INSERT INTO " + currentBuild.get(delayTable) + " " +
                        "(" + currentBuild.get(delayOwnerColumn) + ", " + currentBuild.get(delayIdColumn) + ", "
                        + currentBuild.get(delayCountColumn) + ") " +
                        "VALUES (" + charId + ", " + itemId + ", 1, " + itemEnchant + ")";
            else
                return "INSERT INTO " + currentBuild.get(delayTable) + " " +
                        "(" + currentBuild.get(delayOwnerColumn) + ", " + currentBuild.get(delayIdColumn) + ", " +
                        currentBuild.get(delayCountColumn) + ", " + currentBuild.get(delayEnchantColumn) + ") " +
                        "VALUES (" + charId + ", " + itemId + ", 1, " + itemEnchant + ")";
        else
            return "INSERT INTO " + currentBuild.get(itemsTable) + " " +
                    "(" + currentBuild.get(itemsOwnerColumn) + ", " + currentBuild.get(itemsObjIdColumn) + ", " +
                    currentBuild.get(itemsIdColumn) + ", " + currentBuild.get(itemsCountColumn) + ", " +
                    currentBuild.get(itemsEnchantColumn) + ", " + currentBuild.get(itemsLocColumn) + ", " +
                    currentBuild.get(itemsLocDataColumn) + ") " +
                    "VALUES (" + charId + ", " + nextObjId + ", " + itemId + ", 1, " + itemEnchant + ", 'INVENTORY', -1)";
    }

    public static String addElements(long objId, int elemKey, int elemValue) {
        return "INSERT INTO " + currentBuild.get(elementalTable) + " " +
                "(" + currentBuild.get(elementalItemIdColumn) + ", " + currentBuild.get(elementalTypeColumn) + ", " +
                currentBuild.get(elementalValueColumn) + ") " +
                "VALUES (" + objId + ", " + elemKey + ", " + elemValue + ")";
    }

    public static String updateCount(boolean delayed, String objId, String newValue) {
        String result = "UPDATE ";
        if(delayed)
            return result + currentBuild.get(delayTable) + " " +
                  "SET " + currentBuild.get(delayCountColumn) + " = " + newValue + " " +
                  "WHERE " + currentBuild.get(delayObjIdColumn) + " = " + objId;
        else
            return result + currentBuild.get(itemsTable) + " " +
                    "SET " + currentBuild.get(itemsCountColumn) + " = " + newValue + " " +
                    "WHERE " + currentBuild.get(itemsObjIdColumn) + " = " + objId;
    }

    public static String updateEnchant(boolean delayed, String objId, String newValue) {
        if(delayed)
            return "UPDATE " + currentBuild.get(delayTable) + " " +
                    "SET " + currentBuild.get(delayEnchantColumn) + " = " + newValue + " " +
                    "WHERE " + currentBuild.get(delayObjIdColumn) + " = " + objId;
        else
            return "UPDATE " + currentBuild.get(itemsTable) + " " +
                    "SET " + currentBuild.get(itemsEnchantColumn) + " = " + newValue + " " +
                    "WHERE " + currentBuild.get(itemsObjIdColumn) + " = " + objId;
    }

    public static String deleteItem(boolean delayed, String objId) {
        if(delayed)
            return "DELETE FROM " + currentBuild.get(delayTable) + " " +
                    "WHERE " + currentBuild.get(delayObjIdColumn) + " = " + objId;
        else
            return "DELETE FROM " + currentBuild.get(itemsTable) + " " +
                    "WHERE " + currentBuild.get(itemsObjIdColumn) + " = " + objId;

    }
}

