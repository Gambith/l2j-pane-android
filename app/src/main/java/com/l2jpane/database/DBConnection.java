package com.l2jpane.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.l2jpane.App;
import com.l2jpane.Builds;
import com.l2jpane.R;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.RequestConnected;
import com.l2jpane.singletone.CurrentServer;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;


public class DBConnection {
    private java.sql.Connection con = null;
    private static String currentServer;
    private static Map<String, String> currentData;

    public Map<String, String> readCFG(SharedPreferences sp) {
        return (Map<String, String>) sp.getAll();
    }

    java.sql.Connection getConnect(final Map<String, String> data, final String prefix) throws SQLException, JSchException {

        String host = data.get(prefix + "_DBHost");
        String port = data.get(prefix + "_DBPort");
        String login = data.get(prefix + "_DBLogin");
        String password = data.get(prefix + "_DBPassword");
        String dbname = data.get(prefix + "_DBName");
        if (data.containsKey(prefix + "_ssh") && data.get(prefix + "_ssh").equals("true")) {
            host = "localhost";
            port = getSSHConnect(data, prefix);
        }
        Log.d("connect", "jdbc:mysql://" + host + ":" + port + "/" + dbname + "?characterEncoding=utf-8" + login + password);
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/"
                + dbname + "?characterEncoding=utf-8", login, password);
        return con;
    }

    String getSSHConnect(Map<String, String> data, String server) throws JSchException {
        int localPort = 9980;
        int assinged_port = 0;
        boolean openPort;
        currentServer = server;
        currentData = data;
        Session session = getSSHSession();
            openPort = false;
            while (!openPort) {
                try {
                    assinged_port = session.setPortForwardingL(localPort, data.get(server + "_SSHHost"), Integer.parseInt(data.get(server + "_DBPort")));
                    openPort = true;
                } catch (JSchException e) {
                    localPort++;
                }
            }
        return String.valueOf(assinged_port);
    }

    public static Session getSSHSession() throws JSchException {

        JSch jsch = new JSch();
        Session session = jsch.getSession(currentData.get(currentServer + "_SSHLogin"),
                currentData.get(currentServer + "_SSHHost"), Integer.parseInt(currentData.get(currentServer + "_SSHPort")));
        session.setPassword(currentData.get(currentServer + "_SSHPassword"));
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();
        return session;
    }

    boolean resolveHost(Map<String, String> cfg, String prefix) {
        SocketAddress sockAddr;
        try {
            if (cfg.containsKey(prefix + "_ssh") && cfg.get(prefix + "_ssh").equals("true")) {
                sockAddr = new InetSocketAddress(cfg.get(prefix + "_SSHHost"),
                        Integer.parseInt(cfg.get(prefix + "_SSHPort")));
            } else {
                sockAddr = new InetSocketAddress(cfg.get(prefix + "_DBHost"),
                        Integer.parseInt(cfg.get(prefix + "_DBPort")));
            }
            Socket sock = new Socket();
            int timeoutMs = 20000;
            sock.connect(sockAddr, timeoutMs);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }


}
