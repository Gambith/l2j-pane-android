package com.l2jpane.singletone;

import java.sql.Connection;
import java.util.Map;

/**
 * Created by Aleksey Vishnyakov on 24.06.2016.
 */
public class CurrentServer {
    private static CurrentServer currentServer = new CurrentServer();
    ;

    private Connection loginSQL;
    private Connection gameSQL;
    private String selectedServerName;
    private Map<String, ?> selectedServerCFG;
    private Map<String, String> currentBuild;

    public static CurrentServer getInstance() {
        return currentServer;
    }

    private CurrentServer() {
    }

    public Connection getLoginSQL() {
        return loginSQL;
    }

    public Connection getGameSQL() {
        return gameSQL;
    }

    public String getSelectedServerName() {
        return selectedServerName;
    }

    public Map<String, ?> getSelectedServerCFG() {
        return selectedServerCFG;
    }

    public Map<String, String> getCurrentBuild() {
        return currentBuild;
    }

    public void setLoginSQL(Connection loginSQL) {
        this.loginSQL = loginSQL;
    }

    public void setGameSQL(Connection gameSQL) {
        this.gameSQL = gameSQL;
    }

    public void setSelectedServerName(String selectedServerName) {
        this.selectedServerName = selectedServerName;
    }

    public void setSelectedServerCFG(Map<String, ?> selectedServerCFG) {
        this.selectedServerCFG = selectedServerCFG;
    }

    public void setCurrentBuild(Map<String, String> currentBuild) {
        this.currentBuild = currentBuild;
    }
}
