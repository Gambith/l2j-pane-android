package com.l2jpane;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.l2jpane.dialogs.CheckPassDialog;
import com.l2jpane.dialogs.LoadingDialog;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.RequestConnected;
import com.l2jpane.singletone.CurrentServer;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements CheckPassDialog.CheckPassDialogListener{

    private Snackbar sb;
    private ListView serverLV;
    String[] serverList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fillServerlist();
        if(CheckPassDialog.ts == null || (CheckPassDialog.ts + 300000) < System.currentTimeMillis()) {
            getPassDialog();
            CheckPassDialog.ts = System.currentTimeMillis();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle(R.string.nameandversion);
        }
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CurrentServer.getInstance().setSelectedServerName(null);
                    getSettings();
                }
            });
        }
    }

    private void getPassDialog() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("passdialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the passdialog.
        DialogFragment newFragment = CheckPassDialog.newInstance(0);
        newFragment.setCancelable(false);
        newFragment.show(ft, "passdialog");
    }

    private void fillServerlist() {
        serverList = checkConfigs();
        serverLV = (ListView)findViewById(R.id.serverListView);
        registerForContextMenu(serverLV);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.serverListView) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(serverList[info.position]);

            String[] menuItems = getResources().getStringArray(R.array.menu_serverlist);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu_serverlist);
        String menuItemName = menuItems[menuItemIndex];
        CurrentServer.getInstance().setSelectedServerName(serverList[info.position]);
        if(menuItemName.equals(getString(R.string.connect_server))) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            // Create and show the dialog.
            LoadingDialog newFragment = new LoadingDialog();
            newFragment.setCancelable(false);
            newFragment.show(ft, "loadingdialog");
        } else if (menuItemName.equals(getString(R.string.edit_server))) {
            SharedPreferences serverSP =
                    getSharedPreferences("srv_" + CurrentServer.getInstance().getSelectedServerName(), Context.MODE_PRIVATE);
            SharedPreferences tempSP =
                    getSharedPreferences("com.l2jpane_preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = tempSP.edit();

            Map<String, ?> allEntries = serverSP.getAll();
            for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
                if (entry.getValue().toString().equals("true") || entry.getValue().toString().equals("false"))
                    editor.putBoolean(entry.getKey(), Boolean.parseBoolean(entry.getValue().toString()));
                else
                    editor.putString(entry.getKey(), entry.getValue().toString());
            }
            editor.apply();
            getSettings();
        } else if (menuItemName.equals(getString(R.string.delete_server))) {
            new File("data/data/com.l2jpane/shared_prefs/srv_" + CurrentServer.getInstance().getSelectedServerName() + ".xml").delete();
            fillServerlist();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_CANCELED && this.getCurrentFocus() != null) {
                Snackbar.make(this.getCurrentFocus(), R.string.connection_lost, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

        if (data != null || !data.getStringExtra("shortError").contains("Connection timed out") ||
                !data.getStringExtra("shortError").equals("session is down") ||
                !data.getStringExtra("shortError").contains("after connection closed")) {
            final View.OnClickListener snackbarOnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    final AlertDialog ad;
                    final TextView tv = new TextView(MainActivity.this);
                    tv.setBackgroundColor(0xFF000000);
                    tv.setTextColor(0xFFDD0000);
                    tv.setSelectAllOnFocus(true);
                    tv.setTextSize(9);
                    tv.setText("");
                    Runnable r = new Runnable() {
                        public void run() {
                            for (final char c : data.getStringExtra("fullError").toCharArray()) {
                                final String symbol = String.valueOf(c);
                                SystemClock.sleep(10);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String nString = tv.getText() + symbol;
                                        tv.setText(nString);

                                    }
                                });
                            }
                        }
                    };
                    new Thread(r).start();


                    builder.setIcon(R.drawable.ic_error_black_24dp).setView(tv);
                    builder.setTitle(data.getStringExtra("shortError"))
                            .setPositiveButton(getString(R.string.tell_developer), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent emailIntent = new Intent(Intent.ACTION_SEND);

                                    emailIntent.setType("plain/text");
                                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                                            new String[]{"vishnyakov.aleks@gmail.com"});
                                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                                            "ERROR REPORT: L2jPane Android");
                                    String message = String.format("build: %s \n\n\n Title: %s \n\n Error: %s",
                                            CurrentServer.getInstance().getSelectedServerCFG().get("build"),
                                            data.getStringExtra("shortError"),
                                            data.getStringExtra("fullError"));
                                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                                            message);
                                    emailIntent.setType("message/rfc822");
                                    try {
                                        startActivity(Intent.createChooser(emailIntent, getString(R.string.send_to_developer)));
                                    } catch (android.content.ActivityNotFoundException ex) {
                                        Toast.makeText(MainActivity.this, R.string.no_email_clients, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    ad = builder.create();
                    ad.show();
                }
            };
            sb = Snackbar.make(serverLV, R.string.runtime_error, Snackbar.LENGTH_INDEFINITE);
            sb.setActionTextColor(0xFF00FF00);
            sb.setAction(R.string.yes, snackbarOnClickListener).show();
        }
    }

    private String[] checkConfigs() {
        File serversDir = new File("data/data/com.l2jpane/shared_prefs");
        if(!serversDir.exists()) {
            serversDir.mkdirs();
        }
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.startsWith("srv_");
            }
        };
        String servers [] = serversDir.list(filter);
        for(int i = 0; i < servers.length; i++) {
            servers[i] = servers[i].substring(4, servers[i].length() - 4);
        }
        return servers;

    }

    private void getSettings()
    {
        Intent intent = new Intent(this, SettingsActivity.class);

        startActivity(intent);
    }

    public String app_password = "null";
    @Override
    protected void onResume() {
        BusProvider.getInstance().register(this);
        if(serverList.length != 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.listitem, serverList);
            serverLV.setAdapter(adapter);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Override
    public void onFinishEditDialog(String inputText) {
        app_password = inputText;
    }

    @Subscribe
    public void onRequestConnected(RequestConnected event) {
        CurrentServer.getInstance().setGameSQL(event.getGameSQL());
        CurrentServer.getInstance().setLoginSQL(event.getLoginSQL());
        MainActivity.this.startActivityForResult(new Intent(MainActivity.this, ServerMainActivity.class), 1);
    }
}
