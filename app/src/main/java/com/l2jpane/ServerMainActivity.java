package com.l2jpane;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.l2jpane.database.SQL;
import com.l2jpane.eventbus.BusProvider;
import com.l2jpane.eventbus.ShowErrorEvent;
import com.l2jpane.fragments.BanlistFragment;
import com.l2jpane.fragments.ChatFragment;
import com.l2jpane.fragments.FindFragment;
import com.l2jpane.singletone.CurrentServer;
import com.squareup.otto.Subscribe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Timer;
import java.util.TimerTask;

public class ServerMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final Timer timer = new Timer(true);
    private TextView onlineTV;
    private TextView accountsTV;
    private TextView charactersTV;
    private DrawerLayout drawer;
    private FrameLayout container;
    private ActionBarDrawerToggle toggle;
    private final FindFragment findFragment = new FindFragment();
    private final ChatFragment chatFragment = new ChatFragment();
    private final BanlistFragment banlistFragment = new BanlistFragment();
    private boolean enableSSH;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_server_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        enableSSH = CurrentServer.getInstance().getSelectedServerCFG().get("gs_ssh").equals("true");
        container = (FrameLayout) findViewById(R.id.container);
        if (savedInstanceState == null)
            container.setVisibility(View.VISIBLE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            View navHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_server_main);
            onlineTV = (TextView) navHeaderView.findViewById(R.id.onlineTV);
            accountsTV = (TextView) navHeaderView.findViewById(R.id.accountsTV);
            charactersTV = (TextView) navHeaderView.findViewById(R.id.charactersTV);
            TextView servernameTV = (TextView) navHeaderView.findViewById(R.id.labelHeader);
            servernameTV.setText(CurrentServer.getInstance().getSelectedServerName());
            autoRefreshStats();
            navigationView.setNavigationItemSelectedListener(this);
        }
        WebView wv = (WebView) findViewById(R.id.sponsorsWV);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        wv.loadUrl("http://l2jpane.com/sponsors");
    }

    public void openDrawer(View v) {
        drawer.openDrawer(GravityCompat.START);
        drawer.addDrawerListener(toggle);
    }

    private void autoRefreshStats() {
        class RefreshTask extends TimerTask {
            String[] mCount = new String[3];

            @Override
            public void run() {
                ResultSet resultSet = null;
                try {
                    Statement loginSt = CurrentServer.getInstance().getLoginSQL().createStatement();
                    Statement gameSt = CurrentServer.getInstance().getGameSQL().createStatement();

                    resultSet = gameSt.executeQuery(SQL.getOnline());
                    resultSet.last();
                    mCount[0] = String.valueOf(resultSet.getRow());

                    resultSet = loginSt.executeQuery(SQL.getAccounts());
                    resultSet.last();
                    mCount[1] = String.valueOf(resultSet.getRow());

                    resultSet = gameSt.executeQuery(SQL.getCharacters());
                    resultSet.last();
                    mCount[2] = String.valueOf(resultSet.getRow());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onlineTV.setText(mCount[0]);
                            accountsTV.setText(mCount[1]);
                            charactersTV.setText(mCount[2]);
                        }
                    });
                    loginSt.close();
                    gameSt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    String fullError = "";
                    String shortError = e.getMessage();
                    for (StackTraceElement s : e.getStackTrace()) {
                        fullError += s.toString() + "\n";
                    }
                    BusProvider.getInstance().post(new ShowErrorEvent(shortError, fullError));
                } finally {
                    if (resultSet != null) {
                        try {
                            resultSet.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        timer.schedule(new RefreshTask(), 100, 60000);

    }

    @Subscribe
    public void onShowError(ShowErrorEvent event) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("shortError", event.getShortError());
        returnIntent.putExtra("fullError", event.getFullError());
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    protected void onDestroy() {
        timer.cancel();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        if (fragmentManager.getFragments() != null)
            for (Fragment fragment : fragmentManager.getFragments()) {
                if (fragment != null)
                    fragmentManager.beginTransaction().remove(fragment).commit();
            }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            super.onBackPressed();
        } else if (drawer != null && !drawer.isDrawerOpen(GravityCompat.START) && count == 0) {
            drawer.openDrawer(GravityCompat.START);
            container.setVisibility(View.VISIBLE);
        } else if (count > 0)
            getSupportFragmentManager().popBackStack();



    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        container.setVisibility(View.INVISIBLE);
        int id = item.getItemId();
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        for (int i = 0; i < count; i++)
            getSupportFragmentManager().popBackStack();
        if (id == R.id.find_in_base) {
            fragmentManager.beginTransaction().replace(R.id.frameContainer, findFragment).commit();
        } else if (!enableSSH && (id == R.id.game_chat || id == R.id.banlist)) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.i_cannot))
                    .setIcon(R.drawable.ic_error_black_24dp)
                    .setMessage(getString(R.string.gs_must_be_ssh)).create().show();
        } else if (id == R.id.game_chat) {
            fragmentManager.beginTransaction().replace(R.id.frameContainer, chatFragment).commit();
        } else if (id == R.id.banlist) {
            fragmentManager.beginTransaction().replace(R.id.frameContainer, banlistFragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    protected void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        BusProvider.getInstance().register(this);
        super.onResume();
    }
}
