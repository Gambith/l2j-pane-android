package com.l2jpane;

import android.app.Application;
import android.content.Context;

/**
 * Created by Aleksey Vishnyakov on 26.05.2016.
 */
public class App extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }
}