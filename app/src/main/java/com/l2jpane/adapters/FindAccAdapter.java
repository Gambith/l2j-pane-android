package com.l2jpane.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.l2jpane.R;
import com.l2jpane.obj.SearchResultItem;

import java.util.ArrayList;

public class FindAccAdapter extends BaseAdapter {
    private ArrayList<SearchResultItem> searchArrayList = new ArrayList<>();

    private LayoutInflater mInflater;

    public FindAccAdapter(Context context, ArrayList<SearchResultItem> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return searchArrayList.size();
    }

    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.acc_row_layout, null);
            holder = new ViewHolder();
            holder.loginTV = (TextView) convertView.findViewById(R.id.loginTV);
            holder.ipTV = (TextView) convertView.findViewById(R.id.ipTV);
            holder.emailTV = (TextView) convertView.findViewById(R.id.emailTV);
            holder.hwidTV = (TextView) convertView.findViewById(R.id.hwidTV);
            holder.dateTV = (TextView) convertView.findViewById(R.id.dateTV);
            holder.accRowLayout = (RelativeLayout) convertView.findViewById(R.id.accRowLayout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (getCount() != 0) {
            holder.loginTV.setText(searchArrayList.get(position).getLogin());
            holder.ipTV.setText(searchArrayList.get(position).getIp());
            holder.emailTV.setText(searchArrayList.get(position).getEmail());
            holder.hwidTV.setText(searchArrayList.get(position).getHwid());
            holder.dateTV.setText(String.valueOf(searchArrayList.get(position).getLastAccess()));
            if (searchArrayList.get(position).getAccessLevel() > 0) {
                holder.accRowLayout.setBackgroundColor(0xffaaffaa);
            } else if (searchArrayList.get(position).getAccessLevel() < 0) {
                holder.accRowLayout.setBackgroundColor(0xffffaaaa);
            } else {
                holder.accRowLayout.setBackgroundColor(0);
            }
        }
        return convertView;
    }

    static class ViewHolder {
        TextView loginTV;
        TextView ipTV;
        TextView emailTV;
        TextView hwidTV;
        TextView dateTV;
        RelativeLayout accRowLayout;
    }

}