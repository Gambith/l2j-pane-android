package com.l2jpane.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.l2jpane.R;
import com.l2jpane.obj.SearchResultItem;

import java.util.ArrayList;

public class FindHwidAdapter extends BaseAdapter {
    private ArrayList<SearchResultItem> searchArrayList = new ArrayList<>();

    private LayoutInflater mInflater;

    public FindHwidAdapter(Context context, ArrayList<SearchResultItem> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return searchArrayList.size();
    }

    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.hwid_row_layout, null);
            holder = new ViewHolder();
            holder.loginTV = (TextView) convertView.findViewById(R.id.loginTV);
            holder.hwidTV = (TextView) convertView.findViewById(R.id.hwidTV);
            holder.dateTV = (TextView) convertView.findViewById(R.id.dateTV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (getCount() != 0) {
            holder.loginTV.setText(searchArrayList.get(position).getLogin());
            holder.hwidTV.setText(searchArrayList.get(position).getHwid());
            holder.dateTV.setText(String.valueOf(searchArrayList.get(position).getLastAccess()));
        }
        return convertView;
    }

    static class ViewHolder {
        TextView loginTV;
        TextView hwidTV;
        TextView dateTV;
    }
}