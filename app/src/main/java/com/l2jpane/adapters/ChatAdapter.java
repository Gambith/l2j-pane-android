package com.l2jpane.adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.l2jpane.App;
import com.l2jpane.R;
import com.l2jpane.obj.ChatItem;

import java.util.Calendar;
import java.util.List;

/**
 * Created by aleksey on 23.06.16.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder>{

    private List<ChatItem> chatList;
    private boolean longClickActive = false;
    public ChatAdapter(List<ChatItem> chatList){
        this.chatList = chatList;
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row_layout, parent, false);
        return new ChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ChatViewHolder holder, final int position) {
        holder.channelTV.setText(chatList.get(position).getType());
        holder.timeTV.setText(chatList.get(position).getDate());
        holder.messageTV.setText(String.format("%s: %s", chatList.get(position).getNick(),
                                                        chatList.get(position).getText()));
        holder.messageTV.setTextColor(chatList.get(position).getColor());
        holder.channelTV.setTextColor(chatList.get(position).getColor());
        holder.messageLayout.setOnTouchListener(new View.OnTouchListener() {
            private static final int MIN_CLICK_DURATION = 2000;
            private long startClickTime;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (!longClickActive) {
                            longClickActive = true;
                            startClickTime = Calendar.getInstance().getTimeInMillis();
                        }
                        holder.messageLayout.setAlpha(.1F);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        if (longClickActive) {
                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                            if (clickDuration >= MIN_CLICK_DURATION) {
                                String nick = chatList.get(position).getNick();
                                if (nick.contains(" ")) {
                                    nick = nick.replaceFirst("\\ .*", "");
                                }
                                ClipboardManager clipboard = (ClipboardManager) App.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText("label", nick);
                                clipboard.setPrimaryClip(clip);
                                Toast.makeText(App.getContext(), R.string.nick_copied, Toast.LENGTH_SHORT).show();
                                longClickActive = false;
                            }
                        }
                        longClickActive = false;
                        holder.messageLayout.setAlpha(1);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ChatViewHolder extends RecyclerView.ViewHolder {
        RecyclerView chatRV;
        TextView channelTV;
        TextView timeTV;
        TextView messageTV;
        LinearLayout messageLayout;
        LinearLayout infoLayout;

        ChatViewHolder (View itemView) {
            super(itemView);
            chatRV = (RecyclerView) itemView.findViewById(R.id.chatRV);
            messageLayout = (LinearLayout) itemView.findViewById(R.id.messageLayout);
            infoLayout = (LinearLayout) itemView.findViewById(R.id.infoLayout);
            channelTV = (TextView) itemView.findViewById(R.id.channelTV);
            timeTV = (TextView) itemView.findViewById(R.id.timeTV);
            messageTV = (TextView) itemView.findViewById(R.id.messageTV);
        }
    }
}
