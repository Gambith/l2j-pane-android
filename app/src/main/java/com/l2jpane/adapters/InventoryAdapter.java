package com.l2jpane.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.l2jpane.R;
import com.l2jpane.obj.InventoryItem;

import java.util.ArrayList;

/**
 * Created by aleksey on 08.06.16.
 */
public class InventoryAdapter extends BaseAdapter {
    private ArrayList<InventoryItem> inventoryArrayList = new ArrayList<>();
    private LayoutInflater mInflater;

    public InventoryAdapter(Context context, ArrayList<InventoryItem> results) {
        inventoryArrayList = results;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return inventoryArrayList.size();
    }

    public Object getItem(int position) {
        return inventoryArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.inventory_row_layout, null);
            holder = new ViewHolder();

            holder.itemIdTV = (TextView) convertView.findViewById(R.id.itemIdTV);
            holder.itemIV = (ImageView) convertView.findViewById(R.id.itemIV);
            holder.itemNameTV = (TextView) convertView.findViewById(R.id.itemNameTV);
            holder.itemCountTV = (TextView) convertView.findViewById(R.id.itemCountTV);
            holder.itemElementalTV = (TextView) convertView.findViewById(R.id.itemElementalTV);
            holder.itemLocationTV = (TextView) convertView.findViewById(R.id.itemLocationTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (getCount() != 0) {
            holder.itemIdTV.setText(inventoryArrayList.get(position).getItemId());
            holder.itemNameTV.setText(inventoryArrayList.get(position).getItemName());
            holder.itemCountTV.setText(inventoryArrayList.get(position).getCount());
            holder.itemElementalTV.setText(inventoryArrayList.get(position).getElemental());
            holder.itemLocationTV.setText(inventoryArrayList.get(position).getLocation());
            holder.itemIV.setImageBitmap(inventoryArrayList.get(position).getItemIcon());
        }
        return convertView;
    }

    public class ViewHolder {
        private TextView itemIdTV;
        private TextView itemNameTV;
        private TextView itemCountTV;
        private TextView itemElementalTV;
        private TextView itemLocationTV;
        private ImageView itemIV;
    }
}
