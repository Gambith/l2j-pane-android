package com.l2jpane.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.l2jpane.R;
import com.l2jpane.obj.SearchResultItem;

import java.util.ArrayList;

public class FindCharAdapter extends BaseAdapter {
    private ArrayList<SearchResultItem> searchArrayList = new ArrayList<>();

    private LayoutInflater mInflater;

    public FindCharAdapter(Context context, ArrayList<SearchResultItem> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return searchArrayList.size();
    }

    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.char_row_layout, null);
            holder = new ViewHolder();
            holder.fcLoginTV = (TextView) convertView.findViewById(R.id.fcLoginTV);
            holder.fcNickTV = (TextView) convertView.findViewById(R.id.fcNickTV);
            holder.fcLevelTV = (TextView) convertView.findViewById(R.id.fcLevelTV);
            holder.fcClassTV = (TextView) convertView.findViewById(R.id.fcClassTV);
            holder.fcStatusTV = (TextView) convertView.findViewById(R.id.fcStatusTV);
            holder.fcDateTV = (TextView) convertView.findViewById(R.id.fcDateTV);
            holder.charRowLayout = (RelativeLayout) convertView.findViewById(R.id.charRowLayout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (getCount() != 0) {
            holder.fcLoginTV.setText(searchArrayList.get(position).getLogin());
            holder.fcNickTV.setText(searchArrayList.get(position).getNick());
            holder.fcLevelTV.setText(searchArrayList.get(position).getLevel());
            holder.fcClassTV.setText(searchArrayList.get(position).getProf());
            holder.fcStatusTV.setText(searchArrayList.get(position).getStatus());
            holder.fcDateTV.setText(String.valueOf(searchArrayList.get(position).getLastAccess()));

            if (searchArrayList.get(position).getStatus().equalsIgnoreCase("ONLINE"))
                holder.fcStatusTV.setTextColor(0xff008800);
            else if (searchArrayList.get(position).getStatus().equalsIgnoreCase("OFFLINE"))
                holder.fcStatusTV.setTextColor(0xff880000);
            else
                holder.fcStatusTV.setTextColor(0);

            if (searchArrayList.get(position).getAccessLevel() > 0) {
                holder.charRowLayout.setBackgroundColor(0xffaaffaa);
            } else if (searchArrayList.get(position).getAccessLevel() < 0) {
                holder.charRowLayout.setBackgroundColor(0xffffaaaa);
            } else {
                holder.charRowLayout.setBackgroundColor(0);
            }
        }
        return convertView;
    }

    static class ViewHolder {
        TextView fcLoginTV;
        TextView fcNickTV;
        TextView fcLevelTV;
        TextView fcClassTV;
        TextView fcStatusTV;
        TextView fcDateTV;
        RelativeLayout charRowLayout;
    }
}