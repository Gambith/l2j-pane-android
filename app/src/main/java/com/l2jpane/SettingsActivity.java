package com.l2jpane;


import android.annotation.TargetApi;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.l2jpane.dialogs.SaveServerDialog;
import com.l2jpane.singletone.CurrentServer;

import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {


    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CurrentServer.getInstance().getSelectedServerName() != null) {
            setTitle(CurrentServer.getInstance().getSelectedServerName());
        }
        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || LoginserverPrefFragment.class.getName().equals(fragmentName)
                || GameserverPrefFragment.class.getName().equals(fragmentName);
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    public static class LoginserverPrefFragment extends PreferenceFragment {
        @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_loginserver);
            setHasOptionsMenu(true);
            if (CurrentServer.getInstance().getSelectedServerName() == null) {
                SwitchPreference ls_ssh = (SwitchPreference) findPreference("ls_ssh");
                ls_ssh.setChecked(false);
                SwitchPreference ls_email = (SwitchPreference) findPreference("ls_email");
                ls_email.setChecked(false);
            }
            getActivity().setTitle(getString(R.string.loginserver));
        }
    }

    /**
     * This fragment shows notification preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    public static class GameserverPrefFragment extends PreferenceFragment {
        @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_gameserver);
            setHasOptionsMenu(true);
            if (CurrentServer.getInstance().getSelectedServerName() == null) {
                ListPreference build = (ListPreference) findPreference("build");
                SwitchPreference gs_ssh = (SwitchPreference) findPreference("gs_ssh");
                build.setValue("L2jServer");
                gs_ssh.setChecked(false);
                SwitchPreference gs_HWID_log = (SwitchPreference) findPreference("gs_HWID_log");
                gs_HWID_log.setChecked(false);
            }
            getActivity().setTitle(getString(R.string.gameserver));

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {

            if (!hasNullFields()) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new SaveServerDialog();
                newFragment.show(ft, "settingsdialog");
                return true;
            } else {
                Toast.makeText(SettingsActivity.this, R.string.hasnullfields, Toast.LENGTH_SHORT).show();
            }
        }
        else if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean hasNullFields() {
        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
        String ls_DBHost = preference.getString("ls_DBHost", null);
        String ls_DBLogin = preference.getString("ls_DBLogin", null);
        String ls_DBPassword = preference.getString("ls_DBPassword", null);
        String ls_DBName = preference.getString("ls_DBName", null);

        String gs_DBHost = preference.getString("gs_DBHost", null);
        String gs_DBLogin = preference.getString("gs_DBLogin", null);
        String gs_DBPassword = preference.getString("gs_DBPassword", null);
        String gs_DBName = preference.getString("gs_DBName", null);

        boolean ls_ssh = preference.getBoolean("ls_ssh", false);
        String ls_SSHHost = preference.getString("ls_SSHHost", null);
        String ls_SSHLogin = preference.getString("ls_SSHLogin", null);
        String ls_SSHPassword = preference.getString("ls_SSHPassword", null);

        boolean gs_ssh = preference.getBoolean("gs_ssh", false);
        String gs_SSHHost = preference.getString("gs_SSHHost", null);
        String gs_SSHLogin = preference.getString("gs_SSHLogin", null);
        String gs_SSHPassword = preference.getString("gs_SSHPassword", null);
        if (ls_DBHost == null || ls_DBLogin == null || ls_DBPassword == null || ls_DBName == null ||
                gs_DBHost == null || gs_DBLogin == null || gs_DBPassword == null || gs_DBName == null ||
                ls_DBHost.equals("") || ls_DBLogin.equals("") || ls_DBPassword.equals("") || ls_DBName.equals("") ||
                gs_DBHost.equals("") || gs_DBLogin.equals("") || gs_DBPassword.equals("") || gs_DBName.equals(""))
            return true;
        if (ls_ssh)
            if (ls_SSHHost == null || ls_SSHLogin == null || ls_SSHPassword == null ||
                    ls_SSHHost.equals("") || ls_SSHLogin.equals("") || ls_SSHPassword.equals(""))
                return true;
        if (gs_ssh)
            if (gs_SSHHost == null || gs_SSHLogin == null || gs_SSHPassword == null ||
                    gs_SSHHost.equals("") || gs_SSHLogin.equals("") || gs_SSHPassword.equals(""))
                return true;

        return false;
    }
}

